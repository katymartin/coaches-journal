<?php
/**
 Template Name: Privacy
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

<head>
	<title>Privacy | The Coaches' Journal</title>
	<meta name="description" content="Read our privacy policy.">
</head>


<body>
<main class="cd-main-content">

	<section>
<div class="heroImg" style="background: url('<?php the_field('gen_banner'); ?>'); background-repeat: no-repeat; background-size: cover; background-position: top center; width: 100%; ">
	</div>
 	<div class="homeHeadContent">
 	<h1><?php the_field('gen_title'); ?></h1>
	</div>
</section>

<div id="wrapper">
<section class="privacy">
	<div>
		<p>
		This Privacy Policy describes the policies and procedures of The Coaches’ Journal (“Coaches’ Journal”, “we”, “our” or “us”) on the collection, use and disclosure of your information on www.thecoachesjournal.com (and on coachesjournal.com, as applicable) (the “Site”), social media channels operated by us or on our behalf (the “Social Media Services”), and the services, features, content or applications we offer (collectively with the Site and the Social Media Services, the “Services”). We receive information about you from various sources, including: (i) information you provide to us or through the Site and the Services; (ii) your use of the Services generally; and (iii) from third party websites and services. When you use the Services, you are consenting to the collection, transfer, manipulation, storage, disclosure and other uses of your information as described in this Privacy Policy.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		WHAT DOES THIS PRIVACY POLICY COVER?
		</h3>
		<p>
		This Privacy Policy covers the treatment of personally identifiable information (“Personal Information”) gathered when you are using or accessing the Services. This Privacy Policy also covers our treatment of any Personal Information that our business partners share with us or that we share with our business partners.
		</p>
	</div>

	<div>
		<p>
		This Privacy Policy does not apply to the practices of third parties that we do not own or control, including but not limited to any third party websites, services and applications (“Third Party Services”) that you elect to access through the Service or to individuals that we do not manage or employ. While we attempt to facilitate access only to those Third Party Services that share our respect for your privacy, we cannot take responsibility for the content or privacy policies of those Third Party Services. We encourage you to carefully review the privacy policies of any Third Party Services you access.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		WHAT INFORMATION DO WE COLLECT?
		</h3>
		<p>
		The information we gather enables us to personalize, improve and continue to operate the Services. In connection with certain aspects of the Services, we may request, collect and/or display some of your Personal Information. We collect the following types of information from our users.
		</p>
	</div>


	<div class="privInd">
		<h3 id="lato">
		INFORMATION YOU PROVIDE TO US:
		</h3>
		<p>
		When you provide information to us, such as when you sign up to receive updates and other email communications from us through the Services, you may provide information that could be Personal Information, such as your name and email address. You acknowledge that this information may be personal to you, and providing Personal Information to us, including without limitation by providing your email address in order to receive email updates, you allow others, including us, to identify you and therefore may not be anonymous. By providing us with your email address and other contact information, you acknowledge and agree that we may use your contact information to send you information about our Services, and you expressly consent to receiving email communications from us. You may unsubscribe from marketing and promotional messages by emailing us at unsubscribe@thecoachesjournal.com or clicking on the “unsubscribe” link contained in any such email correspondence, although we, regardless, reserve the right to contact you when we believe it is necessary.
		</p>
	</div>

	<div class="privInd">
		<p>
		Additionally, if you sign in to the Services or any features of the Services through a Third Party Service, for example, by using Facebook Connect to leave comments on articles on the Services, some content and/or information in those accounts (“Third Party Account Information”) may be transmitted into your account with us. By using a Third Party Service in connection with the Services, you hereby acknowledge and agree that Third Party Account Information may be transmitted to us and that such Third Party Account Information transmitted to our Services is covered by this Privacy Policy.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		USER CONTENT:
		</h3>
		<p>
		Some features of the Services allow you to provide content to the Services, such as written comments. All content submitted by you to the Services may be retained by us indefinitely. We may continue to disclose such content to third parties in a manner that does not reveal Personal Information, as described in this Privacy Policy.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		IP ADDRESS INFORMATION AND OTHER INFORMATION COLLECTED AUTOMATICALLY:
		</h3>
		<p>
		We automatically receive and record information from your web browser when you interact with the Services, including your IP address and cookie information. This information is used for fighting spam/malware and also to facilitate collection of data concerning your interaction with the Services (e.g., what links you have clicked on).
		</p>
	</div>

	<div class="privInd">
		<p>
		Generally, the Services automatically collect usage information, such as the number and frequency of visitors to the Site. We may use this data in aggregate form, that is, as a statistical measure, but not in a manner that would identify you personally. This type of aggregate data enables us and third parties authorized by us to figure out how often individuals use parts of the Services so that we can analyze and improve them.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		EMAIL COMMUNICATIONS:
		</h3>
		<p>
		We may receive a confirmation when you open an email from us. We use this confirmation to improve our customer service.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		INFORMATION COLLECTED USING COOKIES:
		</h3>
		<p>
		Cookies are pieces of text that may be provided to your computer through your web browser when you access a website. Your browser stores cookies in a manner associated with each website you visit. We use cookies to enable our servers to recognize your web browser and tell us how and when you visit the Site and otherwise use the Services through the Internet.
		</p>
	</div>

	<div class="privInd">
		<p>
		Our cookies do not, by themselves, contain Personal Information, and we do not combine the general information collected through cookies with other Personal Information to tell us who you are. As noted, however, we do use cookies to identify that your web browser has accessed aspects of the Services and may associate that information with your usage of the Services.
		</p>
	</div>

	<div class="privInd">
		<p>
		Most browsers have an option for turning off the cookie feature, which will prevent your browser from accepting new cookies, as well as (depending on the sophistication of your browser software) allowing you to decide on acceptance of each new cookie in a variety of ways. We strongly recommend that you leave cookies active, because they enable you to take advantage the most attractive features of the Services.
		</p>
	</div>

	<div class="privInd">
		<p>
		This Privacy Policy covers our use of cookies only and does not cover the use of cookies by third parties. We do not control when or how third parties place cookies on your computer. For example, third party websites to which a link points may set cookies on your computer.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		AGGREGATE INFORMATION:
		</h3>
		<p>
		We collect statistical information about how users use the Services (“Aggregate Information”). Some of this information is derived from Personal Information. This statistical information is not Personal Information and cannot be tied back to you or your web browser.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		INFORMATION REGARDING YOUR SOCIAL NETWORKS:
		</h3>
		<p>
		Occasionally, our Services will provide opportunities for users to interact with their accounts on other services, such as Facebook, Twitter, or Instagram (each a “Third Party Account”). For example, you can access posting and sharing tools on the Services, including a “share” or “like” button that allows you to post information to your social networks outside of the Services (“Share”). For example, you can Share articles you like from The Coaches’ Journal with your Facebook friends or Twitter followers. Please note that these tools may be operated by Third Party Services.
		</p>
	</div>

	<div class="privInd">
		<p>
		By using these tools, you acknowledge that some Third Party Account Information may be transmitted to us, and that Third Party Account Information transmitted to our Services is covered by this Privacy Policy. Additionally, when you use one of these sharing tools, the Third Party Service that operates the tool may be collecting information about your browser or online activity through its own tracking technologies and subject to its own privacy policy. Lastly, when you use these tools, some of your information from the Services (such as recent articles you’ve Shared) may be shared with the Third Party Service and others. Therefore, we encourage you to read the privacy policies and other policies of social networks or any other Third Party Services you use in connection with the Services.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		HOW, AND WITH WHOM, IS MY INFORMATION SHARED?
		</h3>
		<p>
		The Services are designed to help you share information with others. As a result, some of the information generated through the Services is shared publicly or with third parties.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		PUBLIC INFORMATION ABOUT YOUR ACTIVITY ON THE SERVICES:
		</h3>
		<p>
		Some of your activity on and through the Services is public by default. This may include, but is not limited to, content you have posted publicly on the Site or otherwise through the Services.
		</p>
	</div>

	<div class="privInd">
		<p>
		Information concerning your use of the Services (such as what pages you have visited) may be tracked anonymously through the use of cookies and stored by us.
		</p>
	</div>

	<div class="privInd">
		<p>
		Please also remember that if you choose to provide Personal Information using certain public features of the Services, then that information is governed by the privacy settings of those particular features and may be publicly available. Individuals reading such information may use or disclose it to other individuals or entities without our control and without your knowledge, and search engines may index that information. We therefore urge you to think carefully about including any specific information you may deem private in content that you create or information that you submit through the Services.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		IP ADDRESS INFORMATION:
		</h3>
		<p>
		While we collect and store IP address information, that information is not made public. We do at times, however, share this information with our partners, service providers and other persons with whom we conduct business, and as otherwise specified in this Privacy Policy.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		INFORMATION YOU ELECT TO SHARE:
		</h3>
		<p>
		You may access other Third Party Services through the Services, for example by clicking on links to those Third Party Services from within the Site. We are not responsible for the privacy policies and/or practices of these Third Party Services, and you are responsible for reading and understanding those Third Party Services’ privacy policies. This Privacy Policy only governs information collected on the Services.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		EMAIL COMMUNICATIONS WITH US:
		</h3>
		<p>
		As part of the Services, you may occasionally receive email and other communications from us.  By providing us with your email address, including without limitation if you sign up for email updates or other communications from us, you expressly consent to receiving such communications.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		INFORMATION SHARED WITH OUR AGENTS:
		</h3>
		<p>
		We employ and contract with people and other entities that perform certain tasks on our behalf and who are under our control (our “Agents”). We may need to share Personal Information with our Agents in order to provide products or services to you. Unless we tell you differently, our Agents do not have any right to use Personal Information or other information we share with them beyond what is necessary to assist us. You hereby consent to our sharing of Personal Information with our Agents.
		</p>
	</div>

	<div>

		<div class="privInd">
		<h3 id="lato">
		INFORMATION DISCLOSED PURSUANT TO BUSINESS TRANSFERS:
		</h3>
		<p>
		In some cases, we may choose to buy or sell assets. In these types of transactions, user information is typically one of the transferred business assets. Moreover, if we, or substantially all of our assets, were acquired, or if we go out of business or enter bankruptcy, user information would be one of the assets that is transferred or acquired by a third party. You acknowledge that such transfers may occur, and that any acquirer of us or our assets may continue to use your Personal Information as set forth in this policy.
		</p>
	</div>

		<div class="privInd">
		<h3 id="lato">
		INFORMATION DISCLOSED FOR OUR PROTECTION AND THE PROTECTION OF OTHERS:
		</h3>
		<p>
		We also reserve the right to access, read, preserve, and disclose any information as we reasonably believe is necessary to (i) satisfy any applicable law, regulation, legal process or governmental request, (ii) enforce this Privacy Policy and our Terms of Service, including investigation of potential violations hereof, (iii) detect, prevent, or otherwise address fraud, security or technical issues, (iv) respond to user support requests, or (v) protect our rights, property or safety, our users and the public. This includes exchanging information with other companies and organizations for fraud protection and spam/malware prevention.
		</p>
	</div>

		<div class="privInd">
		<h3 id="lato">
		INFORMATION WE SHARE WITH YOUR CONSENT:
		</h3>
		<p>
		Except as set forth above, you will be notified when your Personal Information may be shared with third parties, and will be able to prevent the sharing of this information. In addition, if you participate in a promotion involving a promotional partner, your Personal Information (including email address) may be shared with our promotional partners and once shared, will be subject to their applicable privacy policy.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		IS INFORMATION ABOUT ME SECURE?
		</h3>
		<p>
		We store all of our information, including your IP address information, using industry-standard techniques. We do not guarantee or warrant that such techniques will prevent unauthorized access to information about you that we store, Personal Information or otherwise. We seek to protect user information to ensure that it is kept private; however, we cannot guarantee the security of any information. Unauthorized entry or use, hardware or software failure, and other factors, may compromise the security of user information at any time.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		WHAT INFORMATION OF MINE CAN I ACCESS?
		</h3>
		<p>
		You can access and delete cookies through your web browser settings at any time.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		WHAT CHOICES DO I HAVE REGARDING MY INFORMATION?
		</h3>
		<p>
		You can use many of the features of the Services without providing Personal Information, thereby limiting the type of information that we collect. You can always opt not to disclose certain information to us, even though it may be needed to take advantage of some of our features.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		WHAT HAPPENS WHEN THERE ARE CHANGES TO THIS PRIVACY POLICY?
		</h3>
		<p>
		We may amend this Privacy Policy from time to time. Use of information we collect now is subject to the Privacy Policy in effect at the time such information is used. If we make changes in the way we collect or use information, we will notify you by posting an announcement on the Services or sending you an email. A user is bound by any changes to the Privacy Policy when he or she uses the Services after such changes have been first posted.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		WHAT IF I HAVE QUESTIONS OR CONCERNS?
		</h3>
		<p>
		If you have any questions or concerns regarding privacy using the Services, please send us a detailed message to info@thecoachesjournal.com. We will make every effort to resolve your concerns.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		EFFECTIVE DATE:
		</h3>
		<p>
		February 15, 2017
		</p>
	</div>

</section>
</div>
</main>
</body>


<?php get_footer(); ?>

</wrapper>
