<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->



	<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta http-equiv="content-language" content="nl">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">





<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:400,400i,900|Lora:400,700" rel="stylesheet">

		<title><?php wp_title(''); ?></title>

		<?php wp_head(); ?>
			</head>

	<header>
		<div class="cd-logo"><a href="/"><img src="/wp-content/themes/KTBonesTheme/library/images/coachesJournalLogo@2x.png" alt="Logo"></a></div>

		<nav class="cd-main-nav-wrapper">
			<ul class="cd-main-nav">
				<li><a href="/library/">LIBRARY</a></li>
				<li><a href="/blog/">BLOG</a></li>
				<li><a href="/suggest/">SUGGEST</a></li>
				<li><a href="/subscribe/">SUBSCRIBE</a></li>
				<li><a href="/trouble/">TROUBLE</a></li>
				<li><a href="/support/">SUPPORT</a></li>
				<li>
					<a href="#0" class="cd-subnav-trigger"><span>SPORTS</span></a>

					<ul>
						<li class="go-back"><a href="#0"> MENU</a></li>
						<li id="sub-nav"><a href="/baseball/">BASEBALL</a></li>
						<li><a href="/basketball/">BASKETBALL</a></li>
						<li><a href="/football/" class="subPad">FOOTBALL</a></li>
						<li><a href="#0" class="placeholder">Placeholder</a></li>


					</ul>
				</li>
				<li class="social"><a href="https://www.facebook.com/thecoachesjournal/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li class="social"><a href="https://twitter.com/TheCoachJournal" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li class="social"><a href="https://www.instagram.com/thecoachesjournal/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			</ul> <!-- .cd-main-nav -->
		</nav> <!-- .cd-main-nav-wrapper -->

		<a href="#0" class="cd-nav-trigger">Menu<span></span></a>
	</header>
