<?php
/**
 Template Name: Library
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>
<div class="Baseball Posts">
<?php get_header(); ?>
</div>
<head>
	<title>Library | The Coaches' Journal</title>
	<meta name="description" content="See suggested books to read.">
</head>


<body>
<main class="cd-main-content">
<section>
<div class="heroImg" style="background: url('<?php the_field('gen_banner'); ?>'); background-repeat: no-repeat; background-size: cover; background-position: top center; width: 100%; ">
	</div>
 	<div class="homeHeadContent">
 	<h1><?php the_field('gen_title'); ?></h1>
	</div>
</section>

<section class="aboutSec">
<!--
	<div class="featured-wide" style="background: url('<?php the_field('about_banner') ;?>')">
		<div class="featured-quote"><h5><?php the_field('about_quote') ;?></h5>
		<h3 id="lora"><?php the_field('about_quote_author') ;?></h3></div>
	</div>
	-->
	<div id="wrapper">
	<div class="aboutCopy">


	<p><?php the_field('about_copy') ;?></p>
</div>




<section class="libraryWrap">

	<?php
		$args = array(
				'post_type' => 'library',
				'post_status' => 'publish',
				'posts_per_page' => 20,
				'order' => 'DESC'
		);
		$projects_loop = new WP_Query($args);
		if ($projects_loop->have_posts()):
				while ($projects_loop->have_posts()):
					$projects_loop->the_post();

					$bp1 = get_field('book_thumbnail');
					$bp7 = get_field('book_url');
		?>

				<div class="libraryPreview">
				<a  href="<?php echo $bp7; ?>" target="_blank" ><img class="libImg" src="<?php echo $bp1; ?>"></img</a>
				</div>

			<?php
				endwhile;
			wp_reset_postdata();
			endif;
			?>

</section>


</main>

</body>


<?php get_footer(); ?>
