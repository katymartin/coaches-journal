<?php
/*
 * CUSTOM POST TYPE TEMPLATE
 *
 * This is the custom post type post template. If you edit the post type name, you've got
 * to change the name of this template to reflect that name change.
 *
 * For Example, if your custom post type is "register_post_type( 'bookmarks')",
 * then your single template should be single-bookmarks.php
 *
 * Be aware that you should rename 'custom_cat' and 'custom_tag' to the appropiate custom
 * category and taxonomy slugs, or this template will not finish to load properly.
 *
 * For more info: http://codex.wordpress.org/Post_Type_Templates
*/
?>

<?php get_header(); ?>
<main class="cd-main-content">

<div id="blogPost">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>

	<section>
	<div class="heroImg blogHero" style="background: url('<?php the_field('post_p_img'); ?>'); background-repeat: no-repeat; background-size: cover; background-position: top center; width: 100%; ">
		</div>
	</section>


<div class="blogContent">
	<h1><?php the_title();?></h1>
	<h3><?php the_field('post_p_author'); ?> <span id="lora" style="color: grey;">| <?php the_date();?></span></h3>
	<div class="postContent"><?php the_content();?></div>
</div>

</div>




	<?php endwhile; ?>
	<?php endif; ?>
	</div>
</div>
</main>
<?php get_footer(); ?>
