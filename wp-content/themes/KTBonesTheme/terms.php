<?php
/**
 Template Name:Terms
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

<head>
	<title>Terms | The Coaches' Journal</title>
	<meta name="description" content="The terms and conditions between us">
</head>


<body>
<main class="cd-main-content">

<section>
<div class="heroImg" style="background: url('<?php the_field('gen_banner'); ?>'); background-repeat: no-repeat; background-size: cover; background-position: top center; width: 100%; ">
	</div>
 	<div class="homeHeadContent">
 	<h1><?php the_field('gen_title'); ?></h1>
	</div>
</section>


<div id="wrapper">
<section class="terms">

	<div>
		<p>
		Please read these Terms of Service (collectively with The Coaches’ Journal Privacy Policy at www.thecoachesjournal.com/privacy and DMCA Copyright Policy below, the “Terms of Service”) fully and carefully before using www.thecoachesjournal.com or www.coachesjournal.com (collectively, the “ Site”) and the services, features, content or applications offered by The Coaches’ Journal (“Coaches’ Journal”, “we”, “us” or “our”) (together with the Site, the “Services”). These Terms of Service set forth the legally binding terms and conditions for your use of the Site and the Services.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		1. ACCEPTANCE OF TERMS OF SERVICE
		</h3>
		<p>
		A. By using the Services in any manner, including but not limited to visiting or browsing the Site or signing up to receive our subscription newsletter or any other communications, you agree to these Terms of Service and all other operating rules, policies and procedures that may be published from time to time on the Site by us or otherwise through the Services, each of which is incorporated by reference and each of which may be updated from time to time without notice to you.
		</p>
	</div>

	<div>
		<p>
		B. Certain of the Services may be subject to additional terms and conditions specified by us from time to time; your use of such Services is subject to those additional terms and conditions, which are incorporated into these Terms of Service by this reference.
		</p>
	</div>

	<div>
		<p>
		C. These Terms of Service apply to all users of the Services, including, without limitation, users who are contributors of content, information, and other materials or services, registered or otherwise.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		2. ELIGIBILITY
		</h3>
		<p>
		You represent and warrant that you are at least 13 years of age. If you are under age 13, you may not, under any circumstances or for any reason, use the Services. We may, in our sole discretion, refuse to offer the Services to any person or entity and change its eligibility criteria at any time. You are solely responsible for ensuring that these Terms of Service are in compliance with all laws, rules and regulations applicable to you and the right to access the Services is revoked where these Terms of Service or use of the Services is prohibited or to the extent offering, sale or provision of the Services conflicts with any applicable law, rule or regulation. Further, the Services are offered only for your use, and not for the use or benefit of any third party. You must notify us immediately of any change in your eligibility to use the Services, breach of security or any other unauthorized use of the Services you become aware of.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		3. SIGN UP
		</h3>
		<p>
		You can use most parts of the Services without registering, but occasionally there may be features that require you to sign up. When you sign up for any such features, you must provide accurate and complete information and keep such information updated. You may never use another person’s personal information to sign up for any part of the Services without that person’s permission. Additionally, you may be able to access certain parts or features of the Services via Facebook Connect or OAuth (an example of connecting through a “Third Party Account”). By using the Services through a Third Party Account, you permit us to access certain information from such account for use by the Services. You are ultimately in control of how much information is accessible to us and may exercise such control by adjusting your privacy settings on your Third Party Account. You shall have the ability to stop receiving certain of the Services you sign up for by emailing us at unsubscribe@thecoachesjournal.com.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		4. CONTENT
		</h3>
	</div>

	<div class="privInd">
		<h3 id="lato">
		A. DEFINITION
		</h3>
		<p>
		For purposes of these Terms of Service, the term “Content” includes, without limitation articles, photographs, images, illustrations, audio clips, video clips, written posts and comments, software, scripts, graphics, and other information, data, text, and interactive features generated, provided, or otherwise made accessible on or through the Services. For the purposes of this Agreement, “Content” also includes all User Content (as defined below).
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		B. COACHES’ JOURNAL CONTENT
		</h3>
		<p>
		All Content published or otherwise made available through the Services are protected by copyright pursuant to U.S. and international copyright laws, and owned or controlled by The Coaches’ Journal and/or its licensors, as applicable. The Services, including the Site and all Content, are provided solely for your personal, noncommercial use only. You shall abide by all additional copyright notices, information, or restrictions contained in any Content accessed through the Services. You may not modify, publish, transmit, participate in the transfer or sale of, reproduce, create new works from, distribute, perform, display, or in any way exploit, any of the Content or the Services (including software) in whole or in part.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		C. NOTICES AND RESTRICTIONS
		</h3>
		<p>
		In addition to copyright protection, certain Content is protected by trademarks, service marks, patents, trade secrets or other proprietary rights and laws. You shall abide by and maintain all copyright or other notices, information, and restrictions contained in any Content accessed through the Services.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		D. USER LICENSE
		</h3>
		<p>
		Subject to these Terms of Service, we grant each user of the Services a worldwide, non-exclusive, non-sublicensable and non-transferable license to use (i.e., to download and display locally) Content solely for purposes of using the Services. Subject to these Terms of Service, you may copy, distribute, and/or share links to any Content that resides on the Site and/or the Services for your non-commercial use, provided that any such link is accompanied by proper attribution back to The Coaches’ Journal as the source of such Content or Services. Use, reproduction, modification, distribution or storage of any Content for other than purposes of using the Services, including without limitation any copying, sharing, or distribution of any Content in a manner other than linking or as otherwise authorized in these Terms of Service, in whole or in part, is expressly prohibited without prior written permission from us. In order to request such permission, please contact us at legal@thecoachesjournal.com. You shall not sell, license, rent, or otherwise use or exploit any Content for commercial use or in any way that violates any third party right.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		E. USER CONTENT
		</h3>
		<p>
		All Content added, created, uploaded, submitted, distributed, or posted to the Services by users (collectively “User Content”), whether publicly posted or privately transmitted, is the sole responsibility of the person who originated such User Content. You represent that all User Content provided by you is accurate, complete, up-to-date, and in compliance with all applicable laws, rules and regulations. You acknowledge that all Content, including User Content, accessed by you using the Services is at your own risk and you will be solely responsible for any damage or loss to you or any other party resulting therefrom. We do not guarantee that any Content you access on or through the Services is or will continue to be accurate.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		F. LICENSE GRANT
		</h3>
		<p>
		By submitting User Content through the Services, you hereby do and shall grant us a worldwide, non-exclusive, perpetual, royalty-free, fully paid, sublicensable and transferable license to use, edit, modify, truncate, aggregate, reproduce, distribute, prepare derivative works of, display, perform, and otherwise fully exploit the User Content in connection with the Site, the Services and our (and our successors’ and assigns’) businesses, including without limitation for promoting and redistributing part or all of the Site or the Services (and derivative works thereof) in any media formats and through any media channels (including, without limitation, third party websites and feeds), and including after termination of your account or the Services. You also hereby do and shall grant each user of the Site and/or the Services a non-exclusive, perpetual license to access your User Content through the Site and/or the Services, and to use, edit, modify, reproduce, distribute, prepare derivative works of, display and perform such User Content, including after termination of your account or the Services. For clarity, the foregoing license grants to us and our users does not affect your other ownership or license rights in your User Content, including the right to grant additional licenses to your User Content, unless otherwise agreed in writing. You represent and warrant that you have all rights to grant such licenses to us without infringement or violation of any third party rights, including without limitation, any privacy rights, publicity rights, copyrights, trademarks, contract rights, or any other intellectual property or proprietary rights.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		G. AVAILABILITY OF CONTENT
		</h3>
		<p>
		We do not guarantee that any Content will be made available on the Site or through the Services. We reserve the right to, but do not have any obligation to, (i) remove, edit or modify any Content in our sole discretion, at any time, without notice to you and for any reason (including, but not limited to, upon receipt of claims or allegations from third parties or authorities relating to such Content or if we are concerned that you may have violated these Terms of Service), or for no reason at all and (ii) to remove or block any Content from the Services.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		6. RULES OF CONDUCT
		</h3>
	</div>

	<div class="privInd">
		<p>
		A. As a condition of use, you promise not to use the Services for any purpose that is prohibited by these Terms of Service. You are responsible for all of your activity in connection with the Services.
		</p>

		<p>
		B. You shall not (and shall not permit any third party to) either (i) take any action or (ii) upload, download, post, submit or otherwise distribute or facilitate distribution of any Content on or through the Service, including without limitation any User Content, that:
		</p>
		<div class="privInd">

			<p>
			I. infringes any patent, trademark, trade secret, copyright, right of publicity or other right of any other person or entity or violates any law or contractual duty (see our DMCA Copyright Policy below);
			</p>

			<p>
			II. you know is false, misleading, untruthful or inaccurate;
			</p>

			<p>
			III. is unlawful, threatening, abusive, harassing, defamatory, libelous, deceptive, fraudulent, invasive of another’s privacy, tortious, obscene, vulgar, pornographic, offensive, profane, contains or depicts nudity, contains or depicts sexual activity, or is otherwise inappropriate as determined by us in our sole discretion;
			</p>

			<p>
			IV. constitutes unauthorized or unsolicited advertising, junk or bulk e-mail (“spamming”);
			</p>

			<p>
			V. contains software viruses or any other computer codes, files, or programs that are designed or intended to disrupt, damage, limit or interfere with the proper function of any software, hardware, or telecommunications equipment or to damage or obtain unauthorized access to any system, data, password or other information of ours or of any third party;
			</p>

			<p>
			VI. impersonates any person or entity, including any of our employees or representatives; or
			</p>

			<p>
			VII. includes anyone’s identification documents or sensitive financial information.
			</p>
		</div>

		<p>
		C. You shall not: (i) take any action that imposes or may impose (as determined by us in our sole discretion) an unreasonable or disproportionately large load on our (or our third party providers’) infrastructure; (ii) interfere or attempt to interfere with the proper working of the Services or any activities conducted on the Services; (iii) bypass, circumvent or attempt to bypass or circumvent any measures we may use to prevent or restrict access to the Services (or other accounts, computer systems or networks connected to the Services); (iv) run any form of auto-responder or “spam” on the Services; (v) use manual or automated software, devices, or other processes to “crawl” or “spider” any page of the Site; (vi) harvest or scrape any Content from the Services; or (vii) otherwise take any action in violation of our guidelines and policies.
		</p>

		<p>
		D. You shall not (directly or indirectly): (i) decipher, decompile, disassemble, reverse engineer or otherwise attempt to derive any source code or underlying ideas or algorithms of any part of the Services (including without limitation any application), except to the limited extent applicable laws specifically prohibit such restriction, (ii) modify, translate, or otherwise create derivative works of any part of the Services, or (iii) copy, rent, lease, distribute, or otherwise transfer any of the rights that you receive hereunder. You shall abide by all applicable local, state, national and international laws and regulations.
		</p>

		<p>
		E. We also reserve the right to access, read, preserve, and disclose any information as we reasonably believe is necessary to (i) satisfy any applicable law, regulation, legal process or governmental request, (ii) enforce these Terms of Service, including investigation of potential violations hereof, (iii) detect, prevent, or otherwise address fraud, security or technical issues, (iv) respond to user support requests, or (v) protect the rights, property or safety of us, our users and the public.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		7. THIRD PARTY SERVICES
		</h3>
		<p>
		The Services may permit you to link to other websites, services or resources on the Internet (each a “Third Party Service”), and other websites, services or resources may contain links to the Services. When you access third party resources on the Internet, you do so at your own risk. These other resources are not under our control, and you acknowledge that we are not responsible or liable for the content, functions, accuracy, legality, appropriateness or any other aspect of such websites or resources. The inclusion of any such link does not imply our endorsement or any association between us and their operators. You further acknowledge and agree that we shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with the use of or reliance on any such content, goods or services available on or through any such website or resource.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		8. TERMINATION
		</h3>
		<p>
		We may terminate your access to all or any part of the Services at any time, with or without cause, with or without notice, effective immediately, which may result in the forfeiture and destruction of all information associated with your membership. All provisions of these Terms of Service which by their nature should survive termination shall survive termination, including, without limitation, licenses of User Content, ownership provisions, warranty disclaimers, indemnity and limitations of liability.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		9. WARRANTY DISCLAIMER
		</h3>
	</div>

	<div class="privInd">

		<p>
		A. We have no special relationship with or fiduciary duty to you. You acknowledge that we have no duty to take any action regarding:
		</p>

		<div class="privInd">
			<p>
			I. which users gain access to the Services;
			</p>

			<p>
			II. what Content you access via the Services; or
			</p>

			<p>
			III. how you may interpret or use the Content.
			</p>
		</div>

		<p>
		B. You release us from all liability for you having acquired or not acquired Content through the Services. We make no representations concerning any Content contained in or accessed through the Services, and we will not be responsible or liable for the accuracy, copyright compliance, or legality of material or Content contained in or accessed through the Services.
		</p>

		<p>
		C. The services and content are provided “as is”, “as available” and without warranty of any kind, express or implied, including, but not limited to, the implied warranties of title, non-infringement, merchantability and fitness for a particular purpose, and any warranties implied by any course of performance or usage of trade, all of which are expressly disclaimed. We, and our directors, employees, agents, suppliers, partners and content providers do not warrant that: (i) the services will be secure or available at any particular time or location; (ii) any defects or errors will be corrected; (iii) any content or software available at or through the services is free of viruses or other harmful components; or (iv) the results of using the services will meet your requirements. Your use of the services is solely at your own risk.
		</p>

		<p>
		D. You shall and hereby do waive California Civil Code Section 1542 or any other similar law of any jurisdiction, which says in substance: “A general release does not extend to claims which the creditor does not know or suspect to exist in his favor at the time of executing the release, which, if known by him must have materially affected his settlement with the debtor.”
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		10. INDEMNIFICATION
		</h3>
		<p>
		You shall defend, indemnify, and hold harmless us, our affiliates and each of our and their respective employees, contractors, directors, suppliers and representatives from all liabilities, claims, and expenses, including reasonable attorneys’ fees, that arise from or relate to your use or misuse of, or access to, the Services, Content, or otherwise from your User Content, violation of these Terms of Service, or infringement by you, or any third party using your Account or identity in the Services, of any intellectual property or other right of any person or entity. We reserve the right to assume the exclusive defense and control of any matter otherwise subject to indemnification by you, in which event you will assist and cooperate with us in asserting any available defenses.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		11. LIMITATION OF LIABILITY
		</h3>
		<p>
		In no event shall we, nor our directors, employees, agents, partners, suppliers or content providers, be liable under contract, tort, strict liability, negligence or any other legal or equitable theory with respect to the services (i) for any lost profits, data loss, cost of procurement of substitute goods or services, or special, indirect, incidental, punitive, compensatory or consequential damages of any kind whatsoever (however arising), (ii) for any bugs, viruses, trojan horses, or the like (regardless of the source of origination), or (iii) for any direct damages in excess of (in the aggregate) of the greater of (a) fees paid to us for the particular services during the immediately previous three (3) month period or (b) $100.00.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		12. COPYRIGHT DISPUTE POLICY
		</h3>
		<p>
		We have adopted the following general policy toward copyright infringement in accordance with the Digital Millennium Copyright Act (http://lcweb.loc.gov/copyright/legislation/dmca.pdf ). The email address of the Designated Agent to Receive Notification of Claimed Infringement (“Designated Agent”) is listed at the end of this policy.
		</p>
	</div>


	<div class="privInd">
		<h3 id="lato">
		PROCEDURE FOR REPORTING COPYRIGHT INFRINGEMENT:
		</h3>

		<p>
		If you believe that material or content residing on or accessible through our websites, application, or services infringes a copyright, please send a notice of copyright infringement containing the following information to the Designated Agent listed below:
		</p>

		<div class="privInd">
			<p>
			A. A physical or electronic signature of a person authorized to act on behalf of the owner of the copyright that has been allegedly infringed;
			</p>

			<p>
			B. Identification of works or materials being infringed;
			</p>

			<p>
			C. Identification of the material that is claimed to be infringing including information regarding the location of the infringing materials that the copyright owner seeks to have removed, with sufficient detail so that we are capable of finding and verifying its existence;
			</p>

			<p>
			D. Contact information about the notifier including address, telephone number and, if available, e-mail address;
			</p>

			<p>
			E. A statement that the notifier has a good faith belief that the material is not authorized by the copyright owner, its agent, or the law; and
			</p>

			<p>
			F. A statement made under penalty of perjury that the information provided is accurate and the notifying party is authorized to make the complaint on behalf of the copyright owner.
			</p>
		</div>
	</div>

	<div class="privInd">
		<h3 id="lato">
		PROCEDURE TO SUPPLY A COUNTER-NOTICE TO THE DESIGNATED AGENT:
		</h3>
		<p>
		If the content provider, member or user believes that the material that was removed or to which access was disabled is either not infringing, or the content provider, member or user believes that it has the right to post and use such material from the copyright owner, the copyright owner’s agent, or pursuant to the law, the content provider, member or user must send a counter-notice containing the following information to the Designated Agent listed below:
		</p>

		<div class="privInd">
			<p>
			A. A physical or electronic signature of the content provider, member or user;
			</p>

			<p>
			B. Identification of the material that has been removed or to which access has been disabled and the location at which the material appeared before it was removed or disabled;
			</p>

			<p>
			C. A statement that the content provider, member or user has a good faith belief that the material was removed or disabled as a result of mistake or a misidentification of the material; and
			</p>

			<p>
			D. Content provider’s, member’s or user’s name, address, telephone number, and, if available, e-mail address and a statement that such person or entity consents to the jurisdiction of the Federal Court for the judicial district in which the content provider’s, member’s or user’s address is located, or if the content provider’s, member’s or user’s address is located outside the United States, for any judicial district in which The Coaches’ Journal is located, and that such person or entity will accept service of process from the person who provided notification of the alleged infringement.
			</p>

			<p>
			If a counter-notice is received by the Designated Agent, we may send a copy of the counter-notice to the original complaining party informing that person that it may replace the removed material or cease disabling it in 10 business days. Unless the copyright owner files an action seeking a court order against the content provider, member or user, the removed material may be replaced or access to it restored in 10 to 14 business days or more after receipt of the counter-notice, at our discretion.
			</p>

			<p>
			Please contact the Designated Agent to Receive Notification of Claimed Infringement for The Coaches’ Journal at:
			</p>

			<p>legal@thecoachesjournal.com</p>
			<p>Subject: Copyright Agent</p>
		</div>
	</div>


	<div class="privP">
		<h3 id="lato">
		13. GOVERNING LAW AND JURISDICTION
		</h3>
		<p>
		These Terms of Service shall be governed by and construed in accordance with the laws of the State of Wisconsin, including its conflicts of law rules, and the United States of America. You agree that any dispute arising from or relating to the subject matter of these Terms of Service shall be governed by the exclusive jurisdiction and venue of the state and Federal courts of Milwaukee County, Wisconsin.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		14. MODIFICATION
		</h3>
		<p>
		We reserve the right, in our sole discretion, to modify or replace any of these Terms of Service, or change, suspend, or discontinue the Services (including without limitation, the availability of any feature, database, or content) at any time by posting a notice on the Site or by another appropriate means of electronic communication. We may also impose limits on certain features and services or restrict your access to parts or all of the Services without notice or liability. While we will timely provide notice of modifications, it is also your responsibility to check these Terms of Service periodically for changes. Your continued use of the Services following notification of any changes to these Terms of Service constitutes acceptance of those changes.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		15. MISCELLANEOUS
		</h3>
	</div>

	<div class="privInd">
		<h3 id="lato">
		A. ENTIRE AGREEMENT AND SEVERABILITY
		</h3>
		<p>
		These Terms of Service are the entire agreement between you and us with respect to the Services, including use of the Site, and supersede all prior or contemporaneous communications and proposals (whether oral, written or electronic) between you and us with respect to the Services. If any provision of these Terms of Service is found to be unenforceable or invalid, that provision will be limited or eliminated to the minimum extent necessary so that these Terms of Service will otherwise remain in full force and effect and enforceable. The failure of either party to exercise in any respect any right provided for herein shall not be deemed a waiver of any further rights hereunder.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		B. FORCE MAJEURE
		</h3>
		<p>
		We shall not be liable for any failure to perform our obligations hereunder where such failure results from any cause beyond our reasonable control, including, without limitation, mechanical, electronic or communications failure or degradation.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		C. ASSIGNMENT
		</h3>
		<p>
		These Terms of Service are personal to you, and are not assignable, transferable or sublicensable by you except with our prior written consent. We may assign, transfer or delegate any of our rights and obligations hereunder without consent.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		D. AGENCY
		</h3>
		<p>
		No agency, partnership, joint venture, or employment relationship is created as a result of these Terms of Service and neither party has any authority of any kind to bind the other in any respect.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		E. NOTICES
		</h3>
		<p>
		Unless otherwise specified in these Term of Service, all notices under these Terms of Service will be in writing and will be deemed to have been duly given when received, if personally delivered or sent by certified or registered mail, return receipt requested; when receipt is electronically confirmed, if transmitted by facsimile or e-mail; or the day after it is sent, if sent for next day delivery by recognized overnight delivery service. Electronic notices should be sent legal@thecoachesjournal.com.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		F. NO WAIVER
		</h3>
		<p>
		Our failure to enforce any part of these Terms of Service shall not constitute a waiver of our right to later enforce that or any other part of these Terms of Service. Waiver of compliance in any particular instance does not mean that we will waive compliance in the future. In order for any waiver of compliance with these Terms of Service to be binding, we must provide you with written notice of such waiver through one of our authorized representatives.
		</p>
	</div>

	<div class="privInd">
		<h3 id="lato">
		G. HEADINGS
		</h3>
		<p>
		The section and paragraph headings in these Terms of Service are for convenience only and shall not affect their interpretation.
		</p>
	</div>

	<div class="privP">
		<h3 id="lato">
		EFFECTIVE DATE OF TERMS OF SERVICE:
		</h3>
		<p>
		February 15, 2017
		</p>
	</div>



	</section>
</div>
</main>
</body>


<?php get_footer(); ?>

</wrapper>
