<?php
/**
 Template Name: Subscribe
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

<head>
	<title>Subscribe | The Coaches' Journal</title>
	<meta name="description" content="Subscribe to our mailing list.">
</head>


<body>
<main class="cd-main-content">

<section>
<div class="heroImg" style="background: url('<?php the_field('gen_banner'); ?>'); background-repeat: no-repeat; background-size: cover; background-position: top center; width: 100%; ">
	</div>
 	<div class="homeHeadContent">
 	<h1><?php the_field('gen_title'); ?></h1>
	</div>
</section>

<div id="wrapper">

<section class="subscribe"><!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; }
	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup">
<form action="//thecoachesjournal.us15.list-manage.com/subscribe/post?u=aee3476752079618f1718c25e&amp;id=3d0a4bb0f5" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
<div class="mc-field-group">
	<label for="mce-EMAIL"><h3>Email Address</h3>
</label>
	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
<div class="mc-field-group">
	<label for="mce-FNAME"><h3>First Name</h3></label>
	<input type="text" value="" name="FNAME" class="" id="mce-FNAME">
</div>
<div class="mc-field-group">
	<label for="mce-LNAME"><h3>Last Name</h3></label>
	<input type="text" value="" name="LNAME" class="" id="mce-LNAME">
</div>
<div class="mc-field-group input-group">
    <ul><li><input type="checkbox" value="1" name="group[303][1]" id="mce-group[303]-303-0"><label for="mce-group[303]-303-0"><h3 style="display: inline;">Coach</h3></label></li>
<li><input type="checkbox" value="2" name="group[303][2]" id="mce-group[303]-303-1"><label for="mce-group[303]-303-1"><h3 style="display: inline;">Player</h3></label></li>
<li><input type="checkbox" value="4" name="group[303][4]" id="mce-group[303]-303-2"><label for="mce-group[303]-303-2"><h3 style="display: inline;">Fan</h3></label></li>
</ul>
</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_aee3476752079618f1718c25e_3d0a4bb0f5" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
	</section>
</div>
</main>
</body>


<?php get_footer(); ?>

</wrapper>
