<?php
/**
 Template Name: Basketball
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>
<div class="Basketball Posts">
<?php get_header(); ?>
</div>
<head>
	<title>Basketball | The Coaches' Journal</title>
	<meta name="description" content="See basketball related articles recommended by The Coaches' Journal.">
</head>


<body>

<main class="cd-main-content">
		<!-- main content here -->

<section>
<div class="heroImg" style="background: url('<?php the_field('gen_banner'); ?>'); background-repeat: no-repeat; background-size: cover; background-position: top center; width: 100%; ">
	</div>
 	<div class="homeHeadContent">
 	<h1><?php the_field('gen_title'); ?></h1>
	</div>
</section>

<section class="blogWrap">
	<?php
		$args = array(
				'post_type' => 'basketball',
				'post_status' => 'publish',
				'posts_per_page' => 100,
				'order' => 'DESC'
		);
		$projects_loop = new WP_Query($args);
		if ($projects_loop->have_posts()):
				while ($projects_loop->have_posts()):
					$projects_loop->the_post();

						$bp1 = get_field('post_p_img');
						$bp2 = get_field('post_p_author');
						$bp3 = get_field('post_p_publication');
						$bp4 = get_field('source_post_url');
						$bp5 = get_field('post_excerpt');
						$link = get_field('post_link');
          ?>

					<div class="blogPreview">
					<a href="<?php echo $link; ?>" target="_blank">
					<div class="blogPimg" style="background: url('<?php echo $bp1;?>'); background-repeat: no-repeat; background-size: cover; background-position: center center; width: 100%;"></div>
					<h3><span id="lato"><?php echo $bp2;?><span id="lora" style="color:grey;"> | <?php echo $bp3;?></h3>
					<h2><?php the_title();?></h2>
					</a>
					<p><?php echo $bp5;?></p>
					</div>

<?php
	endwhile;
wp_reset_postdata();
endif;
?>

</section>



</main>

</body>


<?php get_footer(); ?>

</wrapper>
