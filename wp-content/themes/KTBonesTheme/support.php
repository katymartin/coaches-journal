<?php
/**
 Template Name: Support
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

<head>
	<title>About the Coaches Journal</title>
	<meta name="about" content="about">
</head>


<body>
<main class="cd-main-content">

	<section>
	<div class="heroImg" style="background: url('<?php the_field('gen_banner'); ?>'); background-repeat: no-repeat; background-size: cover; background-position: top center; width: 100%; ">
		</div>
	 	<div class="homeHeadContent">
	 	<h1><?php the_field('gen_title'); ?></h1>
		</div>
	</section>



<section class="donateSec">

	<div id="wrapper">

		<div class="supportIntro">

		<p><?php the_field('about_copy') ;?></p>
		</div>

<div class="dPlug"><?php echo do_shortcode('[give_form id="623"]'); ?></div>

	</div>
	</section>
</main>
</body>


<?php get_footer(); ?>

</wrapper>
