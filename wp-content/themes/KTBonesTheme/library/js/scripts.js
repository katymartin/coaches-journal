/*
 * Bones Scripts File
 * Author: Eddie Machado
 *
 * This file should contain any js scripts you want to add to the site.
 * Instead of calling it in the header or throwing it inside wp_head()
 * this file will be called automatically in the footer so as not to
 * slow the page load.
 *
 * There are a lot of example functions and tools in here. If you don't
 * need any of it, just remove it. They are meant to be helpers and are
 * not required. It's your world baby, you can do whatever you want.
*/


$(document).ready(function() {
  $(".sideNavSlide").css("height", $("body").height());
});

$(document).ready(function() {
  $(".sideNavSlide").css("height", $("body").height());
});




/*********************
MAKES NAV SLIDE IN
*********************/



$('#nav-toggle').on('click', function(item) {
  $('.navSlide').toggleClass('slide');
})

$('li').on('click', function(e) {
  
})



/*********************
HAMBURGER
*********************/

document.querySelector( "#nav-toggle" )
  .addEventListener( "click", function() {
    this.classList.toggle( "active" );
  });


/*********************
SUBMENU DROP
*********************/

$(document).ready(function(){
			$("a").on('click', function(event) {
				if (this.hash !== "") {
					event.preventDefault();
					var hash = this.hash;
					$('html, body, .content-slot').animate({
						scrollTop: $(hash).offset().top
					}, 500, function(){
						window.location.hash = hash;
					});
				}
			});
  
			$('.sub .sub-menu').hide();
			$('.sub').click(function(){
				$(this).find('.sub-menu').slideToggle();
        
				$(this).toggleClass('open');
			});
		});

/*********************
PUTS NAV IN PLACE TO SLIDE IN
*********************/


$(window).scroll(function() {
// 100 = The point you would like to fade the nav in.
  
	if ($(window).scrollTop() > 100 ){
    
 		$('.sideNav').addClass('right');
    
  } else {
    
    $('.sideNav').removeClass('right');
    
 	};   	
});

$('.scroll').on('click', function(e){		
		e.preventDefault()
    
  $('html, body').animate({
      scrollTop : $(this.hash).offset().top
    }, 1500);
});




$(window).scroll(function() {
// 100 = The point you would like to fade the nav in.
  
	if ($(window).scrollTop() > 100 ){
    
 		$('.bg').addClass('show');
    
  } else {
    
    $('.bg').removeClass('show');
    
 	};   	
});

$('.scroll').on('click', function(e){		
		e.preventDefault()
    
  $('html, body').animate({
      scrollTop : $(this.hash).offset().top
    }, 1500);
});





$("p").each(function() {
  var wordArray = $(this).text().split(" ");
  if (wordArray.length > 1) {
    wordArray[wordArray.length-2] += "&nbsp;" + wordArray[wordArray.length-1];
    wordArray.pop();
    $(this).html(wordArray.join(" "));
  }
});



$('#nav-trigger').click(function (e) 
{

        if($('nav.mobileNav').hasClass('active')) 
            {
                $('nav.mobileNav').removeClass('active');
                $('#nav-trigger').removeClass('active');
                $('#nav-trigger i').removeClass('fa fa-times').addClass('fa fa-bars');
//                $('html').removeClass('noscroll');
//                $('body').removeClass('noscroll');

            }
        else
            {
                $('nav.mobileNav').addClass('active');
                $('#nav-trigger').addClass('active');
                $('#nav-trigger i').removeClass('fa fa-bars').addClass('fa fa-times');
                
//                $('html').addClass('noscroll');
//                $('body').addClass('noscroll');
            }
});





	$('.js-tilt').tilt({
    axis: y
})
	
    const tilt = $('.js-tilt').tilt();
tilt.on('change', callback);  // parameters: event, transforms
tilt.on('tilt.mouseLeave', callback); // parameters: event
tilt.on('tilt.mouseEnter', callback); // parameters: event
	
	const tilt = $('.js-tilt').tilt();

// Destroy instance
tilt.tilt.destroy.call(tilt);

// Get values of instance
tilt.tilt.getValues.call(tilt); // returns [{},{},etc..]

// Reset instance
tilt.tilt.reset.call(tilt);











/*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
*/
function updateViewportDimensions() {
	var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
	return { width:x,height:y };
}
// setting the viewport width
var viewport = updateViewportDimensions();

function loadGravatars() {
  // set the viewport using the function above
  viewport = updateViewportDimensions();
  // if the viewport is tablet or larger, we load in the gravatars
  if (viewport.width >= 768) {
  jQuery('.comment img[data-gravatar]').each(function(){
    jQuery(this).attr('src',jQuery(this).attr('data-gravatar'));
  });
	}
} // end function


/*
 * Put all your regular jQuery in here.
*/
jQuery(document).ready(function($) {

  /*
   * Let's fire off the gravatar function
   * You can remove this if you don't need it
  */
  loadGravatars();


}); /* end of as page load scripts */
