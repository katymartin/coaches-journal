<?php
/**
 Template Name: Blog
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>
<div class="home">
<?php get_header(); ?>
</div>
<head>
	<title>Blog | The Coaches' Journal</title>
	<meta name="description" content="Coaches Journal Home page">
</head>


<body>

<main class="cd-main-content">
<section>
<div class="heroImg" style="background: url('<?php the_field('gen_banner'); ?>'); background-repeat: no-repeat; background-size: cover; background-position: top center; width: 100%; ">
	</div>
 	<div class="homeHeadContent">
 	<h1><?php the_field('gen_title'); ?></h1>
	</div>
</section>


<section class="blogHome">

	<?php
		$args = array(
				'post_type' => 'blog',
				'post_status' => 'publish',
				'posts_per_page' => 100,
				'order' => 'DESC'
		);
		$projects_loop = new WP_Query($args);
		if ($projects_loop->have_posts()):
				while ($projects_loop->have_posts()):
					$projects_loop->the_post();

					$bp1 = get_field('post_p_img');
					$bp2 = get_field('post_p_author');
					$bp3 = get_field('post_p_publication');
					$link = get_the_permalink();
		?>
<a href="<?php echo $link; ?>">
<div class="blogPreview">
<div class="blogPimg" style="background: url('<?php echo $bp1;?>'); background-repeat: no-repeat; background-size: cover; background-position: top center; width: 100%;">
	<div class="hBlogInfo">
		<h3><span id="lato"><?php echo $bp2;?><span id="lora" style="color:grey;"> | <?php echo $bp3;?></h3>
		<h2><?php the_title();?></h2> 
	</div>
</div>
</div>
</a>


<?php
	endwhile;
wp_reset_postdata();
endif;
?>

</section>



</main>

</body>


<?php get_footer(); ?>

</wrapper>
