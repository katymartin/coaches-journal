<?php
/**
 Template Name: Interior
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

<head>
	<title>About the Coaches Journal</title>
	<meta name="about" content="about">
</head>


<body>
<main class="cd-main-content">

<section>
<div class="heroImg" style="background: url('<?php the_field('gen_banner'); ?>'); background-repeat: no-repeat; background-size: cover; background-position: top center; width: 100%; ">
	</div>
 	<div class="homeHeadContent">
 	<h1><?php the_field('gen_title'); ?></h1>
	</div>
</section>

<section class="aboutSec">
<!--
	<div class="featured-wide" style="background: url('< ?php the_field('about_banner') ;?>')">
		<div class="featured-quote"><h5>< ?php the_field('about_quote') ;?></h5>
		<h3 id="lora">< ?php the_field('about_quote_author') ;?></h3></div>
	</div>
	-->
	<div id="wrapper">
	<div class="aboutCopy">

	<p><?php the_field('about_copy') ;?></p>
	</div>




	<div class="aboutContact">

		<?php
  if( have_rows('about_cont') ):
  while( have_rows('about_cont') ): the_row(); ?>

    <div class="aContFlex">
		<div><h3 id ="lora"><?php the_sub_field('a_contact_cat') ;?></h3></div>
		<p><a href="mailto:<?php the_sub_field('a_contact_info_link') ;?>"><?php the_sub_field('a_contact_info') ;?></a></p>
	</div>

<?php endwhile; endif; ?>



	</div>


	<div class="adBlock">

		<?php
	if( have_rows('ad_block') ):
	while( have_rows('ad_block') ): the_row(); ?>

		<div class="adImg">
			<div><h3 id ="lora"><?php the_sub_field('ad_caption') ;?></h3></div>
			<img src="<?php the_sub_field('ad_image') ;?>">
		</div>

<?php endwhile; endif; ?>
	</div>



	</div>
	</section>
</main>
</body>


<?php get_footer(); ?>

</wrapper>
