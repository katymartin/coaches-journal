<?php
/**
 * Plugin Name: Give - 2Checkout Gateway
 * Plugin URI:  https://givewp.com/addons/2checkout-gateway/
 * Description: Give add-on gateway for 2Checkout.
 * Version:     1.0.2
 * Author:      WordImpress
 * Author URI:  https://wordimpress.com
 * Text Domain: give-twocheckout
 * Domain Path: /languages
 *
 * @package: Give
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class Give_Twocheckout
 */
final class Give_Twocheckout {

	/** Singleton *************************************************************/

	/**
	 * Give_Twocheckout instance
	 *
	 * @access private
	 * @since  1.0
	 *
	 * @var    Give_Twocheckout The one true Give_Twocheckout
	 */
	private static $instance;

	/**
	 * The ID
	 *
	 * @access public
	 * @since  1.0
	 *
	 * @var    string
	 */
	public $id = 'give-twocheckout';

	/**
	 * The basename
	 *
	 * @access public
	 * @since  1.0
	 *
	 * @var    string
	 */
	public $basename;

	/**
	 * The admin form
	 *
	 * @access public
	 * @since  1.0
	 *
	 * @var    string Setup objects for each class
	 */
	public $admin_form;

	/**
	 * Main Give_Twocheckout Instance
	 *
	 * Insures that only one instance of Give_Twocheckout exists in memory at any one
	 * time. Also prevents needing to define globals all over the place.
	 *
	 * @staticvar array $instance
	 * @return Give_Twocheckout
	 */
	public static function instance() {
		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Give_Twocheckout ) ) {
			self::$instance = new Give_Twocheckout;
			self::$instance->define_globals();
			self::$instance->includes();
			self::$instance->init();

			// Class Instances.
			self::$instance->payments = new Give_Twocheckout_Payments();

		}

		return self::$instance;
	}

	/**
	 * Defines all the globally used constants
	 *
	 * @since 1.0
	 * @return void
	 */
	private function define_globals() {

		if ( ! defined( 'GIVE_TWOCHECKOUT_VERSION' ) ) {
			define( 'GIVE_TWOCHECKOUT_VERSION', '1.0.2' );
		}
		if ( ! defined( 'GIVE_TWOCHECKOUT_MIN_GIVE_VERSION' ) ) {
			define( 'GIVE_TWOCHECKOUT_MIN_GIVE_VERSION', '1.8.3' );
		}
		if ( ! defined( 'GIVE_TWOCHECKOUT_PLUGIN_FILE' ) ) {
			define( 'GIVE_TWOCHECKOUT_PLUGIN_FILE', __FILE__ );
		}
		if ( ! defined( 'GIVE_TWOCHECKOUT_PLUGIN_DIR' ) ) {
			define( 'GIVE_TWOCHECKOUT_PLUGIN_DIR', dirname( __FILE__ ) );
		}
		if ( ! defined( 'GIVE_TWOCHECKOUT_PLUGIN_URL' ) ) {
			define( 'GIVE_TWOCHECKOUT_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
		}
		if ( ! defined( 'GIVE_TWOCHECKOUT_BASENAME' ) ) {
			define( 'GIVE_TWOCHECKOUT_BASENAME', plugin_basename( __FILE__ ) );
		}

	}

	/**
	 * Initialize.
	 */
	public function init() {
		add_action( 'wp_enqueue_scripts', array( $this, 'scripts' ) );
		add_action( 'give_after_cc_fields', array( $this, 'errors_div' ) );
		load_plugin_textdomain( 'give-twocheckout', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
	}

	/**
	 * Plugin Scripts
	 */
	public function scripts() {

		// Check for sandbox.
		if ( give_is_test_mode() ) {
			$environment = 'sandbox';
		} else {
			$environment = 'production';
		}

		wp_register_script( 'give-twocheckout-tokenization', 'https://www.2checkout.com/checkout/api/2co.min.js', array( 'jquery' ) );
		wp_enqueue_script( 'give-twocheckout-tokenization' );
		wp_register_script( 'give-twocheckout-js', GIVE_TWOCHECKOUT_PLUGIN_URL . '/assets/js/give-twocheckout.js', array(
			'jquery',
			'give-twocheckout-tokenization',
		) );
		wp_enqueue_script( 'give-twocheckout-js' );

		wp_localize_script( 'give-twocheckout-js', 'give_twocheckout_js', array(
			'sellerId'       => give_get_option( 'twocheckout-sellerId' ),
			'publishableKey' => give_get_option( 'twocheckout-public-key' ),
			'env'            => $environment,
			'api_error'      => esc_html__( 'Your payment could not be recorded. Please try again.', 'give-twocheckout' ),
			'keys_error'     => esc_html__( 'Incorrect API account number or keys. Check the plugin settings.', 'give-twocheckout' ),
			'error'          => esc_html__( 'Error', 'give-twocheckout' ),
		) );

	}

	/**
	 * Add an errors div
	 *
	 * @access      public
	 * @since       1.0
	 * @return      void
	 */
	public function errors_div() {
		echo '<div id="give-twocheckout-errors"></div>';
	}

	/**
	 * Include all files
	 *
	 * @since 1.0
	 * @return void
	 */
	private function includes() {
		self::includes_general();
		self::includes_admin();
	}

	/**
	 * Load general files
	 *
	 * @return void
	 */
	private function includes_general() {
		$files = array(
			'class-twocheckout-payments.php',
			'give-twocheckout-helpers.php',
		);

		foreach ( $files as $file ) {
			require( sprintf( '%s/includes/%s', untrailingslashit( GIVE_TWOCHECKOUT_PLUGIN_DIR ), $file ) );
		}
	}

	/**
	 * Load admin files
	 *
	 * @return void
	 */
	private function includes_admin() {
		if ( is_admin() ) {
			$files = array(
				'give-twocheckout-activation.php',
				'give-twocheckout-settings.php',
			);

			foreach ( $files as $file ) {
				require( sprintf( '%s/includes/admin/%s', untrailingslashit( GIVE_TWOCHECKOUT_PLUGIN_DIR ), $file ) );
			}
		}
	}

}

/**
 * The main function responsible for returning the one true Give_Twocheckout
 * Instance to functions everywhere.
 *
 * Use this function like you would a global variable, except without needing
 * to declare the global.
 *
 * @since 1.0
 * @return bool|Give_Twocheckout
 */
function Give_Twocheckout() {
	return Give_Twocheckout::instance();
}

add_action( 'plugins_loaded', 'Give_Twocheckout' );


/**
 * 2Checkout Licensing
 */
function give_add_2checkout_licensing() {
	if ( class_exists( 'Give_License' ) ) {
		new Give_License( __FILE__, '2Checkout Gateway', GIVE_TWOCHECKOUT_VERSION, 'WordImpress', '2checkout_license_key' );
	}
}

add_action( 'plugins_loaded', 'give_add_2checkout_licensing' );

/**
 * Registers the gateway.
 *
 * @param $gateways
 *
 * @return mixed
 */
function give_register_twocheckout_gateway( $gateways ) {
	// Format: ID => Name
	$gateways['twocheckout'] = array(
		'admin_label'    => esc_html__( '2Checkout', 'give-twocheckout' ),
		'checkout_label' => esc_html__( 'Credit Card', 'give-twocheckout' ),
	);

	return $gateways;
}

add_filter( 'give_payment_gateways', 'give_register_twocheckout_gateway' );
