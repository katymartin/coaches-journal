<?php
/**
 * Give 2Checkout Settings
 *
 * @package     Give
 * @copyright   Copyright (c) 2016, WordImpress
 * @license     https://opensource.org/licenses/gpl-license GNU Public License
 * @since       1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Adds the settings to the Payment Gateways section
 *
 * @param $settings
 *
 * @return array
 */
function give_add_twocheckout_settings( $settings ) {

	$give_settings = array(
		array(
			'name' => '<strong>' . __( '2Checkout Gateway', 'give-twocheckout' ) . '</strong>',
			'desc' => '<hr>',
			'type' => 'give_title',
			'id'   => 'give_title_twocheckout',
		),
		array(
			'id'   => 'twocheckout-sellerId',
			'name' => __( 'Account Number', 'give-twocheckout' ),
			'desc' => __( 'Please enter your 2Checkout account number.', 'give-twocheckout' ),
			'type' => 'text'
		),
		array(
			'id'   => 'twocheckout-public-key',
			'name' => __( 'API Publishable Key', 'give-twocheckout' ),
			'desc' => __( 'Please enter your 2Checkout API publishable key.', 'give-twocheckout' ),
			'type' => 'api_key'
		),
		array(
			'id'   => 'twocheckout-private-key',
			'name' => __( 'API Private Key', 'give-twocheckout' ),
			'desc' => __( 'Please enter your 2Checkout API private key.', 'give-twocheckout' ),
			'type' => 'api_key'
		)
	);

	return array_merge( $settings, $give_settings );
}

add_filter( 'give_settings_gateways', 'give_add_twocheckout_settings' );
