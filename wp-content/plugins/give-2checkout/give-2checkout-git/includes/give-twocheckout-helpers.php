<?php
/**
 * Give Twocheckout helper functions
 *
 * @package     Give
 * @copyright   Copyright (c) 2016, WordImpress
 * @license     https://opensource.org/licenses/gpl-license GNU Public License
 * @since       1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 *
 * 2Checkout uses it's own credit card form because the card details are tokenized.
 *
 * We don't want the name attributes to be present on the fields in order to prevent them from getting posted to the
 * server
 *
 * @access      public
 * @since       1.0
 *
 * @param      $form_id
 * @param bool $echo
 *
 * @return string
 */
function give_twocheckout_credit_card_form( $form_id, $echo = true ) {

	ob_start();

	do_action( 'give_before_cc_fields', $form_id ); ?>

    <fieldset id="give_cc_fields-<?php echo $form_id ?>" class="give-do-validate">

        <legend><?php esc_html_e( 'Credit Card Info', 'give-twocheckout' ); ?></legend>

		<?php if ( is_ssl() ) : ?>
            <div id="give_secure_site_wrapper-<?php echo $form_id ?>">
                <span class="give-icon padlock"></span>
                <span><?php esc_html_e( 'This is a secure SSL encrypted payment.', 'give-twocheckout' ); ?></span>
            </div>
		<?php endif; ?>

        <input id="twocheckout-token-<?php echo $form_id ?>" class="twocheckout-token" name="token" type="hidden"
               value="">

        <p id="give-card-number-wrap-<?php echo $form_id ?>" class="form-row form-row-two-thirds">
            <label for="card_number" class="give-label">
				<?php esc_html_e( 'Card Number', 'give-twocheckout' ); ?>
                <span class="give-required-indicator">*</span>
                <span class="give-tooltip give-icon give-icon-question"
                      data-tooltip="<?php esc_attr_e( 'The (typically) 16 digits on the front of your credit card.', 'give-twocheckout' ); ?>"></span>
                <span class="card-type"></span>
            </label>
            <input type="tel" autocomplete="off" id="card_number-<?php echo $form_id ?>" required
                   class="card-number give-input required"
                   placeholder="<?php esc_attr_e( 'Card number', 'give-twocheckout' ); ?>"/>
        </p>

        <p id="give-card-cvc-wrap-<?php echo $form_id ?>" class="form-row form-row-one-third">
            <label for="card_cvc" class="give-label">
				<?php esc_html_e( 'CVC', 'give-twocheckout' ); ?>
                <span class="give-required-indicator">*</span>
                <span class="give-tooltip give-icon give-icon-question"
                      data-tooltip="<?php esc_attr_e( 'The 3 digit (back) or 4 digit (front) value on your card.', 'give-twocheckout' ); ?>"></span>
            </label>
            <input type="tel" size="4" autocomplete="off" id="card_cvc-<?php echo $form_id ?>" required
                   class="card-cvc give-input required"
                   placeholder="<?php esc_attr_e( 'Security code', 'give-twocheckout' ); ?>"/>
        </p>

        <p id="give-card-name-wrap-<?php echo $form_id ?>" class="form-row form-row-two-thirds">
            <label for="card_name" class="give-label">
				<?php esc_html_e( 'Name on the Card', 'give-twocheckout' ); ?>
                <span class="give-required-indicator">*</span>
                <span class="give-tooltip give-icon give-icon-question"
                      data-tooltip="<?php esc_attr_e( 'The name printed on the front of your credit card.', 'give-twocheckout' ); ?>"></span>
            </label>
            <input type="text" autocomplete="off" id="card_name-<?php echo $form_id ?>" required
                   class="card-name give-input required"
                   placeholder="<?php esc_attr_e( 'Card name', 'give-twocheckout' ); ?>"/>
        </p>

		<?php do_action( 'give_before_cc_expiration' ); ?>

        <p class="card-expiration form-row form-row-one-third">
            <label for="card_expiry" class="give-label">
				<?php esc_html_e( 'Expiration (MM/YY)', 'give-twocheckout' ); ?>
                <span class="give-required-indicator">*</span>
                <span class="give-tooltip give-icon give-icon-question"
                      data-tooltip="<?php esc_attr_e( 'The date your credit card expires, typically on the front of the card.', 'give-twocheckout' ); ?>"></span>
            </label>
            <input type="hidden" id="card_exp_month-<?php echo $form_id ?>" class="card-expiry-month"/>
            <input type="hidden" id="card_exp_year-<?php echo $form_id ?>" class="card-expiry-year"/>
            <input type="tel" autocomplete="off" id="card_expiry-<?php echo $form_id ?>" required
                   class="card-expiry give-input required"
                   placeholder="<?php esc_attr_e( 'MM / YY', 'give-twocheckout' ); ?>"/>
        </p>

		<?php do_action( 'give_after_cc_expiration', $form_id ); ?>

    </fieldset>
	<?php

	do_action( 'give_after_cc_fields', $form_id );

	$form = ob_get_clean();

	if ( false !== $echo ) {
		echo $form;
	}

	return $form;
}

add_action( 'give_twocheckout_cc_form', 'give_twocheckout_credit_card_form' );
