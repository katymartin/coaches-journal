<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $give_recurring_authorize;

/**
 * Class Give_Recurring_Authorize
 */
class Give_Recurring_Authorize extends Give_Recurring_Gateway {

	/**
	 * MD5 Hash Value.
	 *
	 * @var string
	 */
	private $md5_hash_value;

	/**
	 * API Login ID.
	 *
	 * @var string
	 */
	private $api_login_id;

	/**
	 * Transaction Key.
	 *
	 * @var string
	 */
	private $transaction_key;

	/**
	 * Sandbox mode.
	 *
	 * @var bool
	 */
	private $is_sandbox_mode;

	/**
	 * Get Authorize started
	 */
	public function init() {

		$this->id = 'authorize';

		// Load Authorize SDK and define its constants.
		$this->load_authnetxml_library();
		$this->define_authorize_values();

		//Cancellation support.
		add_action( 'give_recurring_cancel_authorize_subscription', array( $this, 'cancel_subscription' ), 10, 2 );

		// Add settings.
		add_filter( 'give_settings_gateways', array( $this, 'settings' ) );

		//Require last name.
		add_filter( 'give_donation_form_required_fields', array( $this, 'require_last_name' ), 10, 2 );

	}


	/**
	 * Loads AuthorizeNet PHP SDK.
	 *
	 * @return void
	 */
	public function load_authnetxml_library() {

		$lib = GIVE_RECURRING_PLUGIN_DIR . 'includes/gateways/authorize/AuthnetXML/AuthnetXML.class.php';

		if ( file_exists( $lib ) ) {
			require_once $lib;
		}
	}

	/**
	 * Set API Login ID, Transaction Key and Mode.
	 *
	 * @return void
	 */
	public function define_authorize_values() {

		//Live keys
		if ( ! give_is_test_mode() ) {

			$this->api_login_id    = give_get_option( 'give_api_login' );
			$this->transaction_key = give_get_option( 'give_transaction_key' );
			$this->is_sandbox_mode = false;

		} else {

			//Sandbox keys
			$this->api_login_id    = give_get_option( 'give_authorize_sandbox_api_login' );
			$this->transaction_key = give_get_option( 'give_authorize_sandbox_transaction_key' );
			$this->is_sandbox_mode = true;
		}

		$this->md5_hash_value = give_get_option( 'give_authorize_md5_hash_value' );
	}

	/**
	 * Check that the necessary credentials are set.
	 *
	 * @since 1.3
	 * @return bool
	 */
	private function check_credentials() {
		//Check credentials
		if (
			empty( $this->api_login_id )
			|| empty( $this->transaction_key )
		) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Validates the form data.
	 *
	 * @param $data
	 * @param $posted
	 */
	public function validate_fields( $data, $posted ) {

		if ( ! class_exists( 'AuthnetXML' ) && ! class_exists( 'Give_Authorize' ) ) {
			give_set_error( 'give_recurring_authorize_missing', __( 'The Authorize.net gateway is not activated', 'give-recurring' ) );
		}

		if ( empty( $this->api_login_id ) || empty( $this->transaction_key ) ) {
			give_set_error( 'give_recurring_authorize_settings_missing', __( 'The API Login ID or Transaction key is missing.', 'give-recurring' ) );
		}
	}

	/**
	 * Creates subscription payment profiles and sets the IDs so they can be stored.
	 *
	 * @return bool true on success and false on failure.
	 */
	public function create_payment_profiles() {

		$subscription = $this->subscriptions;
		$card_info    = $this->purchase_data['card_info'];
		$user_info    = $this->purchase_data['user_info'];

		$response = $this->create_authorize_net_subscription( $subscription, $card_info, $user_info );

		if ( $response->isSuccessful() ) {

			$this->subscriptions['profile_id'] = $response->subscriptionId;
			$is_success                        = true;

		} else {

			give_set_error( 'give_recurring_authorize_error', $response->messages->message->code . ' - ' . $response->messages->message->text );
			give_record_gateway_error( 'Authorize.net Error', sprintf( __( 'Gateway Error %1$s: %2$s', 'give-recurring' ), $response->messages->message->code, $response->messages->message->text ) );

			$is_success = false;

		}

		return $is_success;
	}

	/**
	 * Creates a new Automated Recurring Billing (ARB) subscription.
	 *
	 * @param  array $subscription
	 * @param  array $card_info
	 * @param  array $user_info
	 *
	 * @return AuthnetXML
	 */
	public function create_authorize_net_subscription( $subscription, $card_info, $user_info ) {

		$args = $this->generate_create_subscription_request_args( $subscription, $card_info, $user_info );

		// Use AuthnetXML library to create a new subscription request.
		$authnet_xml = new AuthnetXML( $this->api_login_id, $this->transaction_key, $this->is_sandbox_mode );
		$authnet_xml->ARBCreateSubscriptionRequest( $args );

		return $authnet_xml;
	}

	/**
	 * Generates args for making a ARB create subscription request.
	 *
	 * @param  array $subscription
	 * @param  array $card_info
	 * @param  array $user_info
	 *
	 * @return array
	 */
	public function generate_create_subscription_request_args( $subscription, $card_info, $user_info ) {

		// Set date to same timezone as Authorize's servers (Mountain Time) to prevent conflicts
		date_default_timezone_set( 'America/Denver' );
		$today = date( 'Y-m-d' );

		// Calculate totalOccurrences. TODO: confirm if empty or zero
		$total_occurrences = ( $subscription['bill_times'] == 0 ) ? 9999 : $subscription['bill_times'];

		$address = isset( $user_info['address']['line1'] ) ? $user_info['address']['line1'] : '';
		$address .= isset( $user_info['address']['line2'] ) ? ' ' . $user_info['address']['line2'] : '';
		$name = mb_substr( give_recurring_generate_subscription_name( $subscription['id'], $subscription['price_id'] ), 0, 49 );

		$args = array(
			'subscription' => array(
				'name'            => $name,
				'paymentSchedule' => array(
					'interval'         => $this->get_interval( $subscription['period'] ),
					'startDate'        => $today,
					'totalOccurrences' => $total_occurrences,
				),
				'amount'          => $subscription['recurring_amount'],
				'payment'         => array(
					'creditCard' => array(
						'cardNumber'     => $card_info['card_number'],
						'expirationDate' => $card_info['card_exp_year'] . '-' . $card_info['card_exp_month'],
						'cardCode'       => $card_info['card_cvc'],
					)
				),
				'billTo'          => array(
					'firstName' => $user_info['first_name'],
					'lastName'  => $user_info['last_name'],
					'address'   => $address,
					'city'      => isset( $user_info['address']['city'] ) ? $user_info['address']['city'] : '',
					'state'     => isset( $user_info['address']['state'] ) ? $user_info['address']['state'] : '',
					'zip'       => isset( $user_info['address']['zip'] ) ? $user_info['address']['zip'] : '',
					'country'   => isset( $user_info['address']['country'] ) ? $user_info['address']['country'] : '',
				)
			)
		);

		return $args;
	}

	/**
	 * Gets interval length and interval unit for Authorize.net based on Give subscription period.
	 *
	 * @param  string $subscription_period
	 *
	 * @return array
	 */
	public function get_interval( $subscription_period ) {

		$length = '1';
		$unit   = 'days';

		switch ( $subscription_period ) {

			case 'day':
				$unit = 'days';
				break;
			case 'week':
				$length = '7';
				$unit   = 'days';
				break;
			case 'month':
				$length = '1';
				$unit   = 'months';
				break;
			case 'year':
				$length = '12';
				$unit   = 'months';
				break;
		}

		return compact( 'length', 'unit' );
	}

	/**
	 * Determines if the subscription can be cancelled.
	 *
	 * @param  bool              $ret
	 * @param  Give_Subscription $subscription
	 *
	 * @return bool
	 */
	public function can_cancel( $ret, $subscription ) {

		if (
			$subscription->gateway === $this->id
			&& ! empty( $subscription->profile_id )
			&& 'active' === $subscription->status
			&& $this->check_credentials()
		) {
			$ret = true;
		}

		return $ret;

	}

	/**
	 * Determines if the subscription can be cancelled.
	 *
	 * @param  bool              $ret
	 * @param  Give_Subscription $subscription
	 *
	 * @return bool
	 */
	public function can_sync( $ret, $subscription ) {

		if (
			$subscription->gateway === $this->id
			&& $this->check_credentials()
			&& ! empty( $subscription->profile_id )
		) {
			$ret = true;
		}

		return $ret;

	}


	/**
	 * Cancels a subscription.
	 *
	 * @param  Give_Subscription $subscription
	 * @param  bool              $valid
	 *
	 * @return bool|AuthnetXML
	 */
	public function cancel_subscription( $subscription, $valid ) {

		if ( empty ( $valid ) ) {
			return false;
		}

		$response = $this->cancel_authorize_net_subscription( $subscription->profile_id );

		return $response;
	}

	/**
	 * Cancel a ARB subscription based for a given subscription id,
	 *
	 * @param  string $anet_subscription_id
	 *
	 * @return bool
	 */
	public function cancel_authorize_net_subscription( $anet_subscription_id ) {

		// Use AuthnetXML library to create a new subscription request,
		$authnet_xml = new AuthnetXML( $this->api_login_id, $this->transaction_key, $this->is_sandbox_mode );
		$authnet_xml->ARBCancelSubscriptionRequest( array( 'subscriptionId' => $anet_subscription_id ) );

		return $authnet_xml->isSuccessful();
	}

	/**
	 * Processes webhooks from the Authorize.net payment processor.
	 *
	 * @access      public
	 * @since       1.0
	 * @return      void
	 */
	public function process_webhooks() {

		// Sanity checks for listener.
		if ( empty( $_GET['give-listener'] )
		     || ( $this->id !== $_GET['give-listener'] && 'authorizenet' !== $_GET['give-listener'] )
		) {
			return;
		}

		$anet_subscription_id = isset( $_POST['x_subscription_id'] ) ? intval( $_POST['x_subscription_id'] ) : '';

		// Only proceed if we have a sub ID.
		if ( empty( $anet_subscription_id ) ) {
			return;
		}

		// Sanity checks for MD5 Hash Security Option.
		if (
			'on' === give_get_option( 'give_authorize_md5_hash_value_option' )
			&& ! $this->is_silent_post_valid( $_POST )
		) {
			status_header( 400 );
			exit( __( 'Failed to validate Authorize.net MD5-Hash.', 'give-recurring' ) );
		}

		$response_code = isset( $_POST['x_response_code'] ) ? intval( $_POST['x_response_code'] ) : '';
		$reason_code   = isset( $_POST['x_response_reason_code'] ) ? intval( $_POST['x_response_reason_code'] ) : '';
		$message       = __( 'The Authorize.net Silent URL endpoint is ready to accept requests.', 'give-recurring' );

		if ( 1 === $response_code ) {

			// Approved.
			$renewal_amount = sanitize_text_field( $_POST['x_amount'] );
			$transaction_id = sanitize_text_field( $_POST['x_trans_id'] );

			$this->process_approved_transaction( $anet_subscription_id, $renewal_amount, $transaction_id );
			$message = __( 'Processed Authorize.net renewal.', 'give-recurring' );

		} elseif ( 2 === $response_code ) {

			// Declined.

		} elseif ( 3 === $response_code && 8 === $reason_code ) {

			// An expired card.

		}


		status_header( 200 );
		exit( $message );

	}

	/**
	 * Check if the Silent Post is valid via MD5 hash.
	 *
	 * @see: http://stackoverflow.com/questions/20668053/authorize-net-gateway-setup
	 *
	 * @param $request
	 *
	 * @return bool
	 */
	public function is_silent_post_valid( $request ) {

		$auth_md5 = isset( $request['x_MD5_Hash'] ) ? $request['x_MD5_Hash'] : '';

		//Sanity check to ensure we have an MD5 Hash from the silent POST
		if ( empty( $auth_md5 ) ) {
			return false;
		}

		$str           = $this->md5_hash_value . $request['x_trans_id'] . $request['x_amount'];
		$generated_md5 = md5( $str );

		return hash_equals( $generated_md5, $auth_md5 );

	}

	/**
	 * Process approved transaction.
	 *
	 * @param  string $anet_subscription_id
	 * @param  string $amount
	 * @param  string $transaction_id
	 *
	 * @return bool|Give_Subscription
	 */
	public function process_approved_transaction( $anet_subscription_id, $amount, $transaction_id ) {

		$subscription = new Give_Subscription( $anet_subscription_id, true );

		if ( empty( $subscription ) ) {
			return false;
		}

		$args = array(
			'amount'         => $amount,
			'transaction_id' => $transaction_id,
		);

		$subscription->add_payment( $args );
		$subscription->renew();

		return $subscription;
	}

	/**
	 * Register Recurring Authorize Additional settings.
	 *
	 * @access      public
	 * @since       1.0
	 *
	 * @param array $settings
	 *
	 * @return array
	 */
	public function settings( $settings ) {

		$give_authorize_recurring_settings = array(
			array(
				'id'   => 'give_authorize_md5_hash_value_option',
				'name' => __( 'MD5-Hash', 'give-recurring' ),
				'desc' => sprintf( __( 'The Authorize.net MD5 Hash security feature allows Give to authenticate transaction responses from the payment gateway. <a href="%1$s" target="_blank">Read more about this feature &raquo;</a>', 'give-recurring' ), 'http://docs.givewp.com/recurring-authorize-md5hash' ),
				'type' => 'checkbox'
			),
			array(
				'id'          => 'give_authorize_md5_hash_value',
				'row_classes' => 'give-authorize-md5-hash-value',
				'name'        => __( 'Hash Value', 'give-recurring' ),
				'desc'        => __( 'Please type the hash value exactly as it appears within your Authorize.net settings as described in the documentation article linked above.', 'give-recurring' ),
				'type'        => 'api_key'
			)
		);

		return give_settings_array_insert(
			$settings,
			'authorize_collect_billing',
			$give_authorize_recurring_settings
		);
	}

	/**
	 * Link the recurring profile in Authorize.net.
	 *
	 * @since  1.1.2
	 *
	 * @param  string $profile_id   The recurring profile id
	 * @param  object $subscription The Subscription object
	 *
	 * @return string               The link to return or just the profile id.
	 */
	public function link_profile_id( $profile_id, $subscription ) {

		if ( ! empty( $profile_id ) ) {
			$html = '<a href="%s" target="_blank">' . $profile_id . '</a>';

			$payment  = new Give_Payment( $subscription->parent_payment_id );
			$base_url = 'live' === $payment->mode ? 'https://authorize.net/' : 'https://sandbox.authorize.net/';
			$link     = esc_url( $base_url . 'ui/themes/sandbox/ARB/SubscriptionDetail.aspx?SubscrID=' . $profile_id );

			$profile_id = sprintf( $html, $link );
		}

		return $profile_id;

	}


	/**
	 * Require Last Name.
	 *
	 * Authorize requires the last name field be completed and passed when creating subscriptions.
	 *
	 * @since 1.2
	 *
	 * @param $required_fields
	 * @param $form_id
	 *
	 * @return mixed
	 */
	function require_last_name( $required_fields, $form_id ) {

		if ( give_is_gateway_active( $this->id ) && give_is_form_recurring( $form_id ) ) {

			$required_fields['give_last'] = array(
				'error_id'      => 'invalid_last_name',
				'error_message' => __( 'Please enter your last name.', 'give-recurring' )
			);
		}

		return $required_fields;

	}

	/**
	 * Get gateway subscription.
	 *
	 * @since 1.3
	 *
	 * @see   https://github.com/DevinWalker/Authorize.Net-XML/blob/master/examples/arb/ARBGetSubscriptionStatusRequest.php
	 *
	 * @param $subscription Give_Subscription
	 *
	 * @return bool|mixed
	 */
	public function get_subscription_details( $subscription ) {

		$authnet_xml = new AuthnetXML( $this->api_login_id, $this->transaction_key, $this->is_sandbox_mode );

		$authnet_xml->ARBGetSubscriptionRequest( array( 'subscriptionId' => $subscription->profile_id ) );

		//Check for error.
		if ( 'error' == strtolower( $authnet_xml->messages->resultCode ) ) {
			return false;
		}

		$subscription_details = array(
			'status'         => $authnet_xml->subscription->status->__toString(),
			'created'        => strtotime( $authnet_xml->subscription->paymentSchedule->startDate->__toString() ),
			'billing_period' => $this->sync_format_billing_period( $authnet_xml ),
		);

		return $subscription_details;

	}

	/**
	 * Format the billing period for sync.
	 *
	 * @param $authnet_xml
	 *
	 * @return string
	 */
	public function sync_format_billing_period( $authnet_xml ) {

		$length         = (string) $authnet_xml->subscription->paymentSchedule->interval->length;
		$unit           = (string) $authnet_xml->subscription->paymentSchedule->interval->unit;
		$billing_period = '';

		switch ( true ) {
			case ( 'days' === $unit ) :
				$billing_period = 'day';
				break;
			case ( '7' === $length && 'days' === $unit ) :
				$billing_period = 'week';
				break;
			case ( '1' === $length && 'months' === $unit ) :
				$billing_period = 'month';
				break;
			case ( '12' === $length && 'months' === $unit ) :
				$billing_period = 'year';
				break;
		}

		return $billing_period;

	}

	/**
	 * Format the billing period for sync.
	 *
	 * @param $authnet_xml
	 *
	 * @return string
	 */
	public function sync_format_expiration( $authnet_xml ) {

		$expiration = $authnet_xml->subscription->paymentSchedule->startDate->__toString();
		$length     = $authnet_xml->subscription->paymentSchedule->interval->length->__toString();
		$unit       = $authnet_xml->subscription->paymentSchedule->interval->unit->__toString();

		$string = strtotime( $expiration . ' +' . $length . ' ' . $unit );

		return $string;

	}


	/**
	 * Get transactions.
	 *
	 * @see https://community.developer.authorize.net/t5/Integration-and-Testing/Getting-transaction-details/td-p/14198
	 *
	 * @param        $subscription
	 * @param string $date
	 *
	 * @return array|bool
	 */
	public function get_gateway_transactions( $subscription, $date = '' ) {

		$start                 = new DateTime( '1 year ago' );
		$end                   = new DateTime();
		$interval              = new DateInterval( 'P1M' );
		$date_range            = new DatePeriod( $start, $interval, $end );
		$subscription_invoices = array();
		$transactions          = array();

		// Loop through the last 12-months.
		foreach ( $date_range as $date ) {

			$period_start     = $date->format( 'Y-m-d\TH:i:s' );
			$period_end       = $date->modify( '+1 month' )->format( 'Y-m-d\TH:i:s' );
			$authnet_invoices = $this->get_invoices_for_give_subscription( $subscription, $period_start, $period_end );;

			// Authorize.net reporting Transactions API isn't enabled. Show error.
			if ( isset( $authnet_invoices[0] ) && 'E00011' === $authnet_invoices[0] ) {
				$error_message = sprintf( __( 'Access denied. You do not have permissions to call the Authorize.net Transaction Details API. Please <a href="%s" target="_blank">enable the Transactions Detail API</a>.', 'give-recurring' ), 'https://community.developer.authorize.net/t5/Integration-and-Testing/E00011-Access-denied-You-do-not-have-permission-to-call-the/m-p/28676#M15095' );
				Give_Recurring()->synchronizer->print_notice( $error_message, 'error' );
				break;
			}

			//Grab invoices for each month.
			if ( ! empty( $authnet_invoices ) ) {
				array_push( $subscription_invoices, $authnet_invoices );
			}

		}

		// Bundle all invoices into one array formatted for synchronizer.
		foreach ( $subscription_invoices as $invoice_set ) {

			foreach ( $invoice_set as $invoice ) {

				$transactions[ $invoice['transId'] ] = array(
					'amount'         => give_sanitize_amount( $invoice['settleAmount'] ),
					'date'           => strtotime( $invoice['submitTimeLocal'] ),
					'transaction_id' => $invoice['transId'],
				);

			}

		}


		return $transactions;

	}

	/**
	 * Get invoices for Authorize.net subscription.
	 *
	 * @param $subscription
	 * @param $start_date
	 * @param $end_date
	 *
	 * @return array|bool
	 */
	public function get_invoices_for_give_subscription( $subscription, $start_date, $end_date ) {

		$auth = new AuthnetXML( $this->api_login_id, $this->transaction_key, $this->is_sandbox_mode );

		// Get batches from Authorize.net.
		$args = array(
			'includeStatistics'   => true,
			'firstSettlementDate' => $start_date,
			'lastSettlementDate'  => $end_date
		);
		$auth->getSettledBatchListRequest( $args );
		// Check for error.
		if ( 'error' == strtolower( $auth->messages->resultCode ) ) {
			return json_decode( json_encode( (array) $auth->messages->message->code ), true );
		}

		// Create PHP array out of SimpleXML.
		$batches = json_decode( json_encode( (array) $auth->batchList ), true );

		// Need batch to continue.
		if (
			! isset( $batches['batch'] )
			|| ! is_array( $batches['batch'] )
		) {
			return false;
		}

		// Some batches come in in single array without iterator.
		// Here we prepare those batches for our loop below.
		if ( ! isset( $batches['batch'][0] ) ) {
			$batches['batch'] = array( $batches['batch'] );
		}

		$transactions = array();

		// Loop through this batch and pick out subscription's transactions.
		foreach ( $batches['batch'] as $batch ) {

			// Need to get transactions for this specific batch.
			$auth2 = new AuthnetXML( $this->api_login_id, $this->transaction_key, $this->is_sandbox_mode );
			$auth2->getTransactionListRequest( array( 'batchId' => $batch['batchId'] ) );
			$batch_transactions = json_decode( json_encode( (array) $auth2->transactions ), true );

			// Is this a multi-dimensional array of transactions?
			if (
				isset( $batch_transactions['transaction'][0] )
				&& is_array( $batch_transactions['transaction'][0] )
			) {
				// Loop through transactions in batch and check if any are for this subscription.
				foreach ( $batch_transactions['transaction'] as $transaction ) {

					$transactions = $this->setup_batch_transaction_array( $transaction, $transactions, $subscription );

				}

			} else {

				$transactions = $this->setup_batch_transaction_array( $batch_transactions['transaction'], $transactions, $subscription );

			}

		} // End foreach.

		return $transactions;
	}

	/**
	 * Setup batch transactions.
	 *
	 * This function checks conditionally if a transaction is part of the subscription.
	 * If it is then it is added to the subscription transactions' array.
	 *
	 * @param $transaction
	 * @param $transactions
	 * @param $subscription
	 *
	 * @return array
	 */
	function setup_batch_transaction_array( $transaction, $transactions, $subscription ) {

		/**
		 * Add transaction to array if:
		 *
		 * a: There is a subscription ID.
		 * b: If subscription ID's match.
		 * b: This isn't the first payment.
		 */
		if (
			isset( $transaction['subscription']['id'] )
			&& $transaction['subscription']['id'] == $subscription->profile_id
			&& $transaction['subscription']['payNum'] !== '1'
		) {
			$transactions[ $transaction['transId'] ] = $transaction;
		}

		return $transactions;

	}

}


$give_recurring_authorize = new Give_Recurring_Authorize();