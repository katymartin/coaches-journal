<?php
/**
 * Plugin Name: Give - Braintree Gateway
 * Plugin URI:  https://givewp.com/addons/braintree-payment-gateway/
 * Description: Quickly and easily accept credit card donations using your Braintree merchant account.
 * Version:     1.1.1
 * Author:      WordImpress
 * Author URI:  https://wordimpress.com
 * Text Domain: give-braintree
 * Domain Path: /languages
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Plugin constants.
if ( ! defined( 'GIVE_BRAINTREE_VERSION' ) ) {
	define( 'GIVE_BRAINTREE_VERSION', '1.1.1' );
}
if ( ! defined( 'GIVE_BRAINTREE_MIN_GIVE_VERSION' ) ) {
	define( 'GIVE_BRAINTREE_MIN_GIVE_VERSION', '1.8.3' );
}
if ( ! defined( 'GIVE_BRAINTREE_BASENAME' ) ) {
	define( 'GIVE_BRAINTREE_BASENAME', plugin_basename( __FILE__ ) );
}

// Plugin includes
include( dirname( __FILE__ ) . '/includes/admin/give-braintree-activation.php' );
include( dirname( __FILE__ ) . '/includes/admin/give-braintree-admin.php' );

/**
 * Braintree Licensing
 */
function give_add_braintree_licensing() {
	if ( class_exists( 'Give_License' ) ) {
		new Give_License( __FILE__, 'Braintree Gateway', GIVE_BRAINTREE_VERSION, 'WordImpress', 'braintree_license_key' );
	}
}

add_action( 'plugins_loaded', 'give_add_braintree_licensing' );

/**
 * Braintree i18n
 */
function give_braintree_textdomain() {
	// Set filter for plugin's languages directory
	$give_lang_dir = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
	$give_lang_dir = apply_filters( 'give_braintree_languages_directory', $give_lang_dir );

	// Load the translations
	load_plugin_textdomain( 'give-braintree', false, $give_lang_dir );
}

add_action( 'init', 'give_braintree_textdomain' );


/**
 * Get the Braintree API keys.
 *
 * @since 1.1
 */
function give_get_braintree_keys() {

	$keys = array();

	if ( give_is_test_mode() ) {
		// Sandbox.
		$keys['merchant_id']         = trim( give_get_option( 'sandbox_braintree_merchantId' ) );
		$keys['merchant_account_id'] = trim( give_get_option( 'sandbox_braintree_merchantAccountId' ) );
		$keys['public_key']          = trim( give_get_option( 'sandbox_braintree_publicKey' ) );
		$keys['private_key']         = trim( give_get_option( 'sandbox_braintree_privateKey' ) );
	} else {
		// LIVE.
		$keys['merchant_id']         = trim( give_get_option( 'braintree_merchantId' ) );
		$keys['merchant_account_id'] = trim( give_get_option( 'braintree_merchantAccountId' ) );
		$keys['public_key']          = trim( give_get_option( 'braintree_publicKey' ) );
		$keys['private_key']         = trim( give_get_option( 'braintree_privateKey' ) );
	}

	return $keys;

}


/**
 * Braintree Process Payment.
 *
 * @param $purchase_data
 */
function give_braintree_process_payment( $purchase_data ) {

	// Include the sdk.
	$sdk = plugin_dir_path( __FILE__ ) . 'lib/Braintree.php';
	if ( file_exists( $sdk ) ) {
		require_once( $sdk );
	}

	// check the posted cc details.
	$cc       = give_braintree_check_cc_details( $purchase_data );
	$api_keys = give_get_braintree_keys();

	// check for errors before we continue to processing.
	if ( ! give_get_errors() ) {

		// Setup authentication.
		give_braintree_setup_authentication( $api_keys );

		$payment_data = array(
			'price'           => $purchase_data['price'],
			'give_form_title' => $purchase_data['post_data']['give-form-title'],
			'give_form_id'    => intval( $purchase_data['post_data']['give-form-id'] ),
			'give_price_id'   => isset( $purchase_data['post_data']['give-price-id'] ) ? $purchase_data['post_data']['give-price-id'] : '',
			'date'            => $purchase_data['date'],
			'user_email'      => $purchase_data['user_email'],
			'purchase_key'    => $purchase_data['purchase_key'],
			'currency'        => give_get_currency(),
			'user_info'       => $purchase_data['user_info'],
			'status'          => 'pending',
			'gateway'         => 'braintree',
		);

		$payment_id = give_insert_payment( $payment_data );

		$transaction = array(
			'orderId'           => $payment_id,
			'amount'            => $purchase_data['price'],
			'merchantAccountId' => $api_keys['merchant_account_id'],
			'creditCard'        => array(
				'cardholderName'  => $cc['card_name'],
				'number'          => $cc['card_number'],
				'expirationMonth' => $cc['card_exp_month'],
				'expirationYear'  => $cc['card_exp_year'],
				'cvv'             => $cc['card_cvc'],
			),
			'options'           => array(),
		);

		$vault = give_get_option( 'braintree_storeInVaultOnSuccess' );

		if ( $vault ) {
			$transaction['options']['storeInVaultOnSuccess'] = true;
		}

		// If Vault is enabled. Lookup to see if we have an existing customer ID with this donor's email.
		$tokens = give_braintree_get_vault_tokens( $purchase_data );

		if ( isset( $tokens['customer_id'] ) && $vault ) {
			// Repeat customer.
			$transaction['customerId'] = $tokens['customer_id'];
			// Check for duplicate payment cards in vault
			$dup_source = give_braintree_duplicate_payment_source_check( $tokens['customer_id'], $tokens );

			// This CC has already been saved for the customer.
			if ( $dup_source ) {
				// Don't store this one in the vault.
				$transaction['options']['storeInVaultOnSuccess'] = false;
			}
		} else {
			// New customer.
			$transaction['customer'] = array(
				'firstName' => $purchase_data['user_info']['first_name'],
				'lastName'  => $purchase_data['user_info']['last_name'],
				'email'     => $purchase_data['user_email'],
			);
		}

		// Are the billing fields enabled?
		$billing_fields = give_get_option( 'braintree_collect_billing' );
		if ( $billing_fields ) {
			$transaction['billing'] = array(
				'streetAddress'     => $purchase_data['card_info']['card_address'],
				'extendedAddress'   => $purchase_data['card_info']['card_address_2'],
				'locality'          => $purchase_data['card_info']['card_city'],
				'region'            => $purchase_data['card_info']['card_state'],
				'postalCode'        => $purchase_data['card_info']['card_zip'],
				'countryCodeAlpha2' => $purchase_data['card_info']['card_country'],
			);
		}

		$settlement = give_get_option( 'braintree_submitForSettlement' );
		if ( $settlement ) {
			$transaction['options']['submitForSettlement'] = true;
		}

		// Filter for other devs.
		$transaction = apply_filters( 'give_braintree_transaction_args', $transaction );

		// Try the Braintree sale:
		try {

			$result = Braintree_Transaction::sale( $transaction );

			// Successful result?
			if ( isset( $result->success ) && ! empty( $result->success ) ) {

				give_braintree_update_vault_tokens( $result, $purchase_data );
				give_update_payment_status( $payment_id, 'complete' );
				give_set_payment_transaction_id( $payment_id, $result->transaction->_attributes['id'] );
				give_send_to_success_page();

			} elseif ( isset( $result->errors ) && sizeof( $result->errors ) > 0 ) {

				// Handle API response errors
				give_braintree_handle_transaction_errors( $result, $payment_id );

			}
		} catch ( Exception $e ) {

			// Log all exceptions.
			give_log_braintree_error( $e );

		}
	}

}

add_action( 'give_gateway_braintree', 'give_braintree_process_payment' );

/**
 * Handle Braintree API Transaction errors.
 *
 * @since 1.1
 */
function give_braintree_handle_transaction_errors( $result, $payment_id ) {

	// Transaction errors from declined payments, etc.
	if ( isset( $result->transaction->processorResponseCode ) && $result->transaction->processorResponseCode > 0 ) {

		give_set_error( 'braintree_error', esc_html__( 'There was a problem processing your credit card. Please double check your payment information and try again.', 'give-braintree' ) );
		// Log with DB.
		give_record_gateway_error( esc_html__( 'Braintree Error', 'give-braintree' ), sprintf( __( 'An error happened while processing a donation.<br><br>Details: %1$s <br><br>Code: %2$s', 'give-braintree' ), $result->transaction->processorResponseText, $result->transaction->processorResponseCode ) );

	} else {
		// Errors returned by Braintree API
		// @see https://developers.braintreepayments.com/reference/general/validation-errors/overview/php
		foreach ( $result->errors->deepAll() as $error ) {

			give_set_error( 'braintree_error', $error->attribute . ' #' . $error->code . ' - ' . $error->message );
			// Log with DB.
			give_record_gateway_error( esc_html__( 'Braintree Error', 'give-braintree' ), sprintf( __( 'An error happened while processing a donation.<br><br>Details: %1$s <br><br>Code: %2$s', 'give-braintree' ), $error->message, $error->attribute ) );

		}
	}

	// Mark payment as failed.
	give_update_payment_status( $payment_id, 'failed' );

	// Send donor on back.
	give_send_back_to_checkout( '?payment-mode=braintree' );

}

/**
 * Check for duplicate payment source in Vault.
 *
 * @param $customer_id
 */
function give_braintree_duplicate_payment_source_check( $customer_id, $tokens ) {

	$found = false;

	try {
		$customer = Braintree_Customer::find( $customer_id );

		// Loop through CC results.
		foreach ( $customer->creditCards as $cc ) {

			// If CC token found in db, this is a duplicate payment method.
			if ( in_array( $cc->token, $tokens['customer_cards'] ) ) {

				return true;
			}
		}
	} catch ( Exception $e ) {
		give_log_braintree_error( $e );
	}

	return $found;

}


/**
 * Log Braintree exception.
 *
 * @see   https://developers.braintreepayments.com/reference/general/exceptions/php
 *
 * @since 1.1
 *
 * @param $exception Braintree_Exception|Exception
 */
function give_log_braintree_error( $exception ) {

	$generic_message = esc_html__( 'An error occurred during processing of the donation.', 'give-braintree' );

	switch ( $exception ) {

		case ( $exception instanceof Braintree_Exception_Authentication ):
			$error_message = sprintf( esc_html__( 'A gateway authentication issue occurred while processing this donation. Details: %s', 'give-braintree' ), $exception->getMessage() );
			break;
		case ( $exception instanceof Braintree_Exception_Configuration ):
			$error_message = sprintf( esc_html__( 'A gateway configuration issue occurred while processing this donation. Details: %s', 'give-braintree' ), $exception->getMessage() );
			break;
		case ( $exception instanceof Braintree_Exception_Authorization ):
			$error_message = sprintf( esc_html__( 'A gateway authorization issue occurred while processing this donation. Details: %s', 'give-braintree' ), $exception->getMessage() );
			break;
		case ( $exception instanceof Braintree_Exception_DownForMaintenance ):
			$error_message = sprintf( esc_html__( 'The payment gateway is down for maintenance at this time. Please try your donation again in a few hours. Details: %s', 'give-braintree' ), $exception->getMessage() );
			break;
		case ( $exception instanceof Braintree_Exception_SSLCertificate ):
			$error_message = sprintf( esc_html__( 'The payment gateway could not verify this site\'s SSL certificate. Details: %s', 'give-braintree' ), $exception->getMessage() );
			break;
		case ( $exception instanceof Braintree_Exception_Unexpected ):
			$error_message = esc_html__( 'An error occurred at payment gateway while processing your donation. Please try again.', 'give-braintree' );
			break;
		case ( $exception instanceof Braintree_Exception_ServerError ):
			$error_message = esc_html__( 'An error occurred at payment gateway while processing your donation. Please try again.', 'give-braintree' );
			break;
		case ( $exception instanceof Braintree_Exception_InvalidSignature ):
			$error_message = sprintf( esc_html__( 'The payment gateway returned an invalid signature response. Details: %s', 'give-braintree' ), $exception->getMessage() );
			break;
		case ( $exception instanceof Braintree_Exception_NotFound ):
			$error_message = sprintf( esc_html__( 'The record you are trying to locate could not be found. Details: %s', 'give-braintree' ), $exception->getMessage() );
			break;
		case ( $exception instanceof Braintree_Exception_ForgedQueryString ):
			$error_message = $generic_message . ' ' . sprintf( esc_html__( 'Details: %s', 'give-braintree' ), $exception->getMessage() );
			break;
		default:
			$error_message = $generic_message . ' ' . esc_html__( 'Please try again.', 'give-braintree' );

	}

	// Log with DB.
	give_record_gateway_error( esc_html__( 'Braintree Error', 'give-braintree' ), sprintf( __( 'An error happened while processing a donation.<br><br>Details: %1$s <br><br>Code: %2$s', 'give-braintree' ), $exception->getMessage(), $exception->getCode() ) );

	// Display error for user.
	give_set_error( 'braintree_error', $error_message );

	// Send em' on back
	give_send_back_to_checkout( '?payment-mode=braintree' );

}

/**
 * Check CC Details
 *
 * @param $purchase_data
 *
 * @return array
 */
function give_braintree_check_cc_details( $purchase_data ) {
	$keys = array(
		'card_number'    => esc_html__( 'credit card number', 'give-braintree' ),
		'card_exp_month' => esc_html__( 'expiration month', 'give-braintree' ),
		'card_exp_year'  => esc_html__( 'expiration year', 'give-braintree' ),
		'card_name'      => esc_html__( 'card holder name', 'give-braintree' ),
		'card_cvc'       => esc_html__( 'security code', 'give-braintree' ),
	);

	$cc_details = array();

	foreach ( $keys as $key => $desc ) {
		if ( ! isset( $_POST[ $key ] ) || empty( $_POST[ $key ] ) ) {
			give_set_error( 'bad_' . $key, sprintf( esc_html__( 'You must enter a valid %s.', 'give-braintree' ), $desc ) );
			give_send_back_to_checkout();
		} else {
			$data = esc_textarea( trim( $_POST[ $key ] ) );
			switch ( $key ) {
				case 'card_exp_month':
					$data = str_pad( $data, 2, 0, STR_PAD_LEFT );
					break;
				case 'card_exp_year':
					if ( strlen( $data ) > 2 ) {
						$data = substr( $data, - 2 );
					}
					break;
			}
			$cc_details[ $key ] = $data;

		}
	}

	return $cc_details;
}

/**
 * Given a transaction ID, generate a link to the Braintree transaction ID details
 *
 * @since  1.0
 *
 * @param  string $transaction_id The Transaction ID
 * @param  int    $payment_id     The payment ID for this transaction
 *
 * @return string                 A link to the PayPal transaction details
 */
function give_braintree_link_transaction_id( $transaction_id, $payment_id ) {

	if ( $transaction_id == $payment_id ) {
		return $transaction_id;
	}

	$base = 'https://';

	if ( 'test' == get_post_meta( $payment_id, '_give_payment_mode', true ) ) {
		$base .= 'sandbox.';
	}

	$base            .= 'braintreegateway.com/merchants/' . give_get_option( 'braintree_merchantId' ) . '/transactions/';
	$transaction_url = '<a href="' . esc_url( $base . $transaction_id ) . '" target="_blank">' . $transaction_id . '</a>';

	return apply_filters( 'give_braintree_link_payment_details_transaction_id', $transaction_url );

}

add_filter( 'give_payment_details_transaction_id-braintree', 'give_braintree_link_transaction_id', 10, 2 );


/**
 * Support for Braintree's "Vault" feature
 *
 * @since 1.1
 *
 * @param $result
 * @param $purchase_data
 */
function give_braintree_update_vault_tokens( $result, $purchase_data ) {

	// Customer meta: Save donor data in Braintree vault if user email present.
	if ( class_exists( 'Give_DB_Customer_Meta' ) && ! empty( $purchase_data['user_email'] ) ) {

		$customer = new Give_Customer( $purchase_data['user_email'] );

		$tokens = $customer->get_meta( 'give_braintree_cc_tokens' );

		if ( empty( $tokens ) ) {
			$tokens = give_braintree_set_vault_token_array( $result );
		}

		// Update customer meta.
		$customer->update_meta( 'give_braintree_cc_tokens', $tokens );

	} elseif ( isset( $purchase_data['user_info']['id'] ) && $purchase_data['user_info']['id'] > 0 ) {

		$tokens = get_user_meta( $purchase_data['user_info']['id'], 'give_braintree_cc_tokens', true );

		if ( empty( $tokens ) ) {
			$tokens = give_braintree_set_vault_token_array( $result );
		}

		// Update user meta.
		update_user_meta( $purchase_data['user_info']['id'], 'give_braintree_cc_tokens', $tokens );

	}

}

/**
 * Vault token array creation.
 *
 * @param $result
 *
 * @return array|bool
 */
function give_braintree_set_vault_token_array( $result ) {

	if ( ! isset( $result->transaction->customerDetails->id ) ) {
		return false;
	}

	if ( ! isset( $result->transaction->creditCardDetails->token ) ) {
		return false;
	}

	$tokens = array(
		'customer_id'    => $result->transaction->customerDetails->id,
		'customer_cards' => array( $result->transaction->creditCardDetails->token ),
	);

	return $tokens;

}

/**
 * Get vault tokens from customer or user meta.
 *
 * Returns an array if successful containing customer ID and CC tokens.
 *
 * @param $purchase_data
 *
 * @return bool|mixed
 */
function give_braintree_get_vault_tokens( $purchase_data ) {

	if ( class_exists( 'Give_DB_Customer_Meta' ) && ! empty( $purchase_data['user_email'] ) ) {

		$give_customer = new Give_Customer( $purchase_data['user_email'] );
		$tokens        = $give_customer->get_meta( 'give_braintree_cc_tokens' );

	} elseif ( isset( $purchase_data['user_info']['id'] ) && $purchase_data['user_info']['id'] > 0 ) {

		// Update user meta.
		$tokens = get_user_meta( $purchase_data['user_info']['id'], 'give_braintree_cc_tokens' );

	}

	if ( ! empty( $tokens ) ) {
		return $tokens;
	} else {
		return false;
	}

}

/**
 * Optional Billing Fields
 *
 * @since 1.1
 *
 * @param $form_id
 *
 * @return void
 */
function give_braintree_optional_billing_fields( $form_id ) {

	// Remove Address Fields if user has option enabled
	if ( ! give_get_option( 'braintree_collect_billing' ) ) {
		remove_action( 'give_after_cc_fields', 'give_default_cc_address_fields' );
	}

	// Ensure CC field is in place properly
	do_action( 'give_cc_form', $form_id );

}


add_action( 'give_braintree_cc_form', 'give_braintree_optional_billing_fields', 10, 1 );


/**
 * Perform automatic upgrades when necessary.
 *
 * @since 1.1
 *
 * @return void
 */
function give_braintree_automatic_upgrades() {
	$did_upgrade      = false;
	$previous_version = preg_replace( '/[^0-9.].*/', '', get_option( 'give_braintree_version' ) );

	// Version 1.1 upgrade.
	if ( version_compare( '1.1', $previous_version, '>' ) || empty( $previous_version ) ) {

		// Sets the default option to display Billing Details
		// as to not mess with any donation forms without consent.
		give_update_option( 'braintree_collect_billing', 'on' );
		$did_upgrade = true;
	}

	if ( $did_upgrade || version_compare( $previous_version, GIVE_BRAINTREE_VERSION, '<' ) ) {
		// Update the version # saved in DB after version checks above.
		update_option( 'give_braintree_version', GIVE_BRAINTREE_VERSION );
	}
}

add_action( 'admin_init', 'give_braintree_automatic_upgrades' );


/**
 * Setup Braintree authentication
 *
 * @param $api_keys
 */
function give_braintree_setup_authentication( $api_keys ) {
	try {
		if ( give_is_test_mode() ) {
			Braintree_Configuration::environment( 'sandbox' );
		} else {
			Braintree_Configuration::environment( 'production' );
		}
		Braintree_Configuration::merchantId( $api_keys['merchant_id'] );
		Braintree_Configuration::publicKey( $api_keys['public_key'] );
		Braintree_Configuration::privateKey( $api_keys['private_key'] );

	} catch ( Exception $e ) {

		give_log_braintree_error( $e );

	}
}
