<?php
/**
 * Admin Functions
 *
 * @package     Give
 * @copyright   Copyright (c) 2015, WordImpress
 * @license     https://opensource.org/licenses/gpl-license GNU Public License
 * @since       1.2
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Register Braintree Gateway
 *
 * @since 1.0
 *
 * @param array $gateways
 *
 * @return array
 */
function give_braintree_register_gateway( $gateways ) {
	$gateways['braintree'] = array(
		'admin_label'    => esc_html__( 'Braintree', 'give-braintree' ),
		'checkout_label' => esc_html__( 'Credit Card', 'give-braintree' )
	);

	return $gateways;
}

add_filter( 'give_payment_gateways', 'give_braintree_register_gateway' );

/**
 * Register the gateway settings
 *
 * @since 1.0
 *
 * @param array $settings
 *
 * @return array
 */
function give_braintree_add_settings( $settings ) {

	$gateway_settings = array(
		array(
			'id'   => 'braintree_settings',
			'name' => __( 'Braintree', 'give-braintree' ),
			'desc' => '<hr>',
			'type' => 'give_title',
		),
		array(
			'id'   => 'braintree_merchantId',
			'name' => __( 'Live Merchant ID', 'give-braintree' ),
			'desc' => __( 'Enter your LIVE unique merchant ID (found on the portal under API Keys when you first login).', 'give-braintree' ),
			'type' => 'text',
			'size' => 'regular',
		),
		array(
			'id'   => 'braintree_merchantAccountId',
			'name' => __( 'Live Merchant Account ID', 'give-braintree' ),
			'desc' => __( 'Enter your LIVE unique merchant account ID (found under Settings > Processing > Merchant Accounts).', 'give-braintree' ),
			'type' => 'text',
		),
		array(
			'id'   => 'braintree_publicKey',
			'name' => __( 'Live Public Key', 'give-braintree' ),
			'desc' => __( 'Enter your LIVE public key (found on the portal under API Keys when you first login).', 'give-braintree' ),
			'type' => 'text',
		),
		array(
			'id'   => 'braintree_privateKey',
			'name' => __( 'Live Private Key', 'give-braintree' ),
			'desc' => __( 'Enter your LIVE private key (found on the portal under API Keys when you first login).', 'give-braintree' ),
			'type' => 'api_key',
		),
		array(
			'id'   => 'sandbox_braintree_merchantId',
			'name' => __( 'Sandbox Merchant ID', 'give-braintree' ),
			'desc' => __( 'Enter your unique sandbox merchant ID (found on the portal under API Keys when you first login).', 'give-braintree' ),
			'type' => 'text',
			'size' => 'regular',
		),
		array(
			'id'   => 'sandbox_braintree_merchantAccountId',
			'name' => __( 'Sandbox Merchant Account ID', 'give-braintree' ),
			'desc' => __( 'Enter your sandbox unique merchant account ID (found under Settings > Processing > Merchant Accounts).', 'give-braintree' ),
			'type' => 'text',
		),
		array(
			'id'   => 'sandbox_braintree_publicKey',
			'name' => __( 'Sandbox Public Key', 'give-braintree' ),
			'desc' => __( 'Enter your sandbox public key (found on the portal under API Keys when you first login).', 'give-braintree' ),
			'type' => 'text',
		),
		array(
			'id'   => 'sandbox_braintree_privateKey',
			'name' => __( 'Sandbox Private Key', 'give-braintree' ),
			'desc' => __( 'Enter your sandbox private key (found on the portal under API Keys when you first login).', 'give-braintree' ),
			'type' => 'api_key',
		),
		array(
			'id'   => 'braintree_collect_billing',
			'name' => __( 'Collect Billing Details', 'give-braintree' ),
			'desc' => __( 'This option will enable the billing details section for Braintree which requires the donor\'s address to complete the donation. These fields are not required by Braintree to process the transaction, but you may have the need to collect the data.', 'give-braintree' ),
			'type' => 'checkbox',
		),
		array(
			'id'   => 'braintree_submitForSettlement',
			'name' => __( 'Submit for Settlement', 'give-braintree' ),
			'desc' => __( 'Enable this option if you would like to immediately submit all transactions for settlement. This will charge the donation without needing your prior approval.', 'give-braintree' ),
			'type' => 'checkbox',
		),
		array(
			'id'   => 'braintree_storeInVaultOnSuccess',
			'name' => __( 'Store In Vault on Success', 'give-braintree' ),
			'desc' => sprintf( __( 'Enable this option if you would like to store the customers <strong>payment information</strong> in the Braintree Vault upon a successful donation. <a href="%s" target="_blank">Read more about Braintree\'s Vault feature &raquo;</a>', 'give-braintree' ), 'https://articles.braintreepayments.com/control-panel/vault/overview' ),
			'type' => 'checkbox',
		),
	);

	return array_merge( $settings, $gateway_settings );
}

add_filter( 'give_settings_gateways', 'give_braintree_add_settings' );
