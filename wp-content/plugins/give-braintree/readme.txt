=== Give - Braintree Gateway ===
Contributors: wordimpress
Tags: donations, donation, ecommerce, e-commerce, fundraising, fundraiser, braintree, gateway
Requires at least: 4.2
Tested up to: 4.7.3
Stable tag: 1.1.1
License: GPLv3
License URI: https://opensource.org/licenses/GPL-3.0

Braintree Gateway Add-on for Give

== Description ==

This plugin requires the Give plugin activated to function properly. When activated, it adds a payment gateway for Braintree.

== Installation ==

= Minimum Requirements =

* WordPress 4.2 or greater
* PHP version 5.3 or greater
* MySQL version 5.0 or greater
* Some payment gateways require fsockopen support (for IPN access)

= Automatic installation =

Automatic installation is the easiest option as WordPress handles the file transfers itself and you don't need to leave your web browser. To do an automatic install of Give, log in to your WordPress dashboard, navigate to the Plugins menu and click Add New.

In the search field type "Give" and click Search Plugins. Once you have found the plugin you can view details about it such as the the point release, rating and description. Most importantly of course, you can install it by simply clicking "Install Now".

= Manual installation =

The manual installation method involves downloading our donation plugin and uploading it to your server via your favorite FTP application. The WordPress codex contains [instructions on how to do this here](http://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation).

= Updating =

Automatic updates should work like a charm; as always though, ensure you backup your site just in case.

== Changelog ==

= 1.1.1 =
* Fix: When a custom donation was given the gateway would incorrectly assign it as a donation level within the receipt despite the correct custom amount being processed.

= 1.1 =
* New: Added the ability to disable the "Billing Details" field set for to optimize donations forms by having the fewest number of fields possible to accept credit cards.
* New: Separated fields for sandbox and live credentials so admins can more easily switch between live and test mode.
* New: Overhauled how the plugin validates and displays payment gateway errors. They are now much more verbose and also logged in the database and viewable by admins under WP-Admin > Donations > Reports > Logs > Pay Errors.

= 1.0.1 =
* Fixed an issue with validation errors
* Updated the Braintree API libraries

= 1.0 =
* Initial plugin release. Yippee!

