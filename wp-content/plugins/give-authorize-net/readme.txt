=== Give - Authorize.net Gateway ===
Contributors: wordimpress
Tags: donations, donation, ecommerce, e-commerce, fundraising, fundraiser, authorize.net, gateway
Requires at least: 4.2
Tested up to: 4.7.3
Stable tag: 1.2.3
License: GPLv3
License URI: https://opensource.org/licenses/GPL-3.0

Authorize.net Gateway Add-on for Give.

== Description ==

This plugin requires the Give plugin activated to function properly. When activated, it adds a payment gateway for Authorize.net.

== Installation ==

= Minimum Requirements =

* WordPress 4.2 or greater
* PHP version 5.3 or greater
* MySQL version 5.0 or greater
* Some payment gateways require fsockopen support (for IPN access)

= Automatic installation =

Automatic installation is the easiest option as WordPress handles the file transfers itself and you don't need to leave your web browser. To do an automatic install of Give, log in to your WordPress dashboard, navigate to the Plugins menu and click Add New.

In the search field type "Give" and click Search Plugins. Once you have found the plugin you can view details about it such as the the point release, rating and description. Most importantly of course, you can install it by simply clicking "Install Now".

= Manual installation =

The manual installation method involves downloading our donation plugin and uploading it to your server via your favorite FTP application. The WordPress codex contains [instructions on how to do this here](http://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation).

= Updating =

Automatic updates should work like a charm; as always though, ensure you backup your site just in case.

== Changelog ==

= 1.2.3 =
* New: Improvements to plugin activation checks and general code optimization.
* Fix: When a custom donation was given the gateway would incorrectly assign it as a donation level within the receipt despite the correct custom amount being processed.

= 1.2.3 =
* Fix: Verify with Authorize.net via API response that the account is properly configured in order to accept a donation. Resolves "Transactions of this market type cannot be processed on this system." error messages.
* Fix: Plugin is now compatible with the unofficial Authorize.net WooCommerce Add-on: https://github.com/WordImpress/Give-Authorize-Gateway/issues/40

= 1.2.1 =
* Fix: License field not displaying due to action being invoked at an improper time - https://github.com/WordImpress/Give-Authorize-Gateway/issues/21
* Update: Removed test & doc directories from the Authorize.net SDK for a lighter plugin footprint - https://github.com/WordImpress/Give-Authorize-Gateway/issues/19

= 1.2 =
* New: Added the ability to disable "Billing Details" fieldset for Authorize.net to optimize donations forms with the least amount of fields possible - https://github.com/WordImpress/Give-Authorize-Gateway/issues/16
* Fix: Set the frontend "Payment Method" value for [donation_receipt] to "Credit Card" rather than "authorizenet" - https://github.com/WordImpress/Give-Authorize-Gateway/issues/15

= 1.1 =
* New: Class structure introduced for better organization and code quality
* New: Updated API to latest SDK
* New: Separate sandbox credential settings for easier testing
* New: Plugin activation banner with links to support and docs
* Fix: Bug with connecting to Authorizenet for some users

= 1.0 =
* Initial plugin release. Yippee!

