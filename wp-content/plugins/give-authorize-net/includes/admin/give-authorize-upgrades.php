<?php
/**
 * Authorize.net Upgrades
 *
 * @package     Give
 * @copyright   Copyright (c) 2017, WordImpress
 * @license     https://opensource.org/licenses/gpl-license GNU Public License
 * @since       1.2
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Give_Authorize_Upgrades
 */
class Give_Authorize_Upgrades {
	
	/**
	 * Give_Authorize_Upgrades constructor.
	 */
	public function __construct() {

		//Activation
		register_activation_hook( GIVE_AUTHORIZE_PLUGIN_FILE, array( $this, 'version_check' ) );

	}

	/**
	 * Version check
	 */
	public function version_check() {

		$previous_version = get_option( 'give_authorize_version' );

		//No version option saved
		if ( version_compare( '1.2', $previous_version, '>' ) || empty( $previous_version ) ) {
			$this->update_v12_default_billing_fields();
		}

		//Update the version # saved in DB after version checks above
		update_option( 'give_authorize_version', GIVE_AUTHORIZE_VERSION );

	}

	/**
	 * Update 1.2 Collect Billing Details
	 *
	 * @description: Sets the default option to display Billing Details as to not mess with any donation forms without consent
	 * @see        : https://github.com/WordImpress/Give-Authorize-Gateway/issues/16
	 */
	private function update_v12_default_billing_fields() {

		give_update_option( 'authorize_collect_billing', 'on' );

	}

}

new Give_Authorize_Upgrades();