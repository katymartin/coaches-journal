<?php
/**
 * Give Authorize Settings
 *
 * @package     Give
 * @copyright   Copyright (c) 2017, WordImpress
 * @license     https://opensource.org/licenses/gpl-license GNU Public License
 * @since       1.1
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// adds the settings to the Payment Gateways section
function give_add_authorize_settings( $settings ) {

	$give_settings = array(
		array(
			'name' => '<strong>' . __( 'Authorize.net Gateway', 'give-authorize-net' ) . '</strong>',
			'desc' => '<hr>',
			'type' => 'give_title',
			'id'   => 'give_title_authorize_net',
		),
		array(
			'id'   => 'give_api_login',
			'name' => esc_html__( 'Live API Login ID', 'give-authorize-net' ),
			'desc' => esc_html__( 'Please enter your authorize.net API login ID.', 'give-authorize-net' ),
			'type' => 'text'
		),
		array(
			'id'   => 'give_transaction_key',
			'name' => esc_html__( 'Live Transaction Key', 'give-authorize-net' ),
			'desc' => esc_html__( 'Please enter your authorize.net transaction key.', 'give-authorize-net' ),
			'type' => 'text'
		),
		array(
			'id'   => 'give_authorize_sandbox_api_login',
			'name' => esc_html__( 'Sandbox API Login ID', 'give-authorize-net' ),
			'desc' => __( 'Please enter your <em>sandbox</em> authorize.net API login ID for testing purposes.', 'give-authorize-net' ),
			'type' => 'text'
		),
		array(
			'id'   => 'give_authorize_sandbox_transaction_key',
			'name' => esc_html__( 'Sandbox Transaction Key', 'give-authorize-net' ),
			'desc' => __( 'Plase enter your <em>sandbox</em> authorize.net transaction key for testing purposes.', 'give-authorize-net' ),
			'type' => 'text'
		),
		array(
			'name' => esc_html__( 'Collect Billing Details', 'give-authorize-net' ),
			'desc' => sprintf( esc_html__( 'This option will enable the billing details section for Authorize which requires the donor\'s address to complete the donation. These fields are not required by Authorize.net to process the transaction, but you may have the need to collect the data.', 'give-authorize-net' ) ),
			'id'   => 'authorize_collect_billing',
			'type' => 'checkbox'
		),
	);

	return array_merge( $settings, $give_settings );
}

add_filter( 'give_settings_gateways', 'give_add_authorize_settings' );
