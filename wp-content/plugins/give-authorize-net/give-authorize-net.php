<?php
/**
 * Plugin Name: Give - Authorize.net Gateway
 * Plugin URI:  https://givewp.com/addons/authorize-net-gateway/
 * Description: Give add-on gateway for Authorize.net
 * Version:     1.2.3
 * Author:      WordImpress
 * Author URI:  https://wordimpress.com
 * Text Domain: give-authorize-net
 * Domain Path: /languages
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Constants
if ( ! defined( 'GIVE_AUTHORIZE_VERSION' ) ) {
	define( 'GIVE_AUTHORIZE_VERSION', '1.2.3' );
}
if ( ! defined( 'GIVE_AUTHORIZE_MIN_GIVE_VERSION' ) ) {
	define( 'GIVE_AUTHORIZE_MIN_GIVE_VERSION', '1.8.3' );
}
if ( ! defined( 'GIVE_AUTHORIZE_PLUGIN_FILE' ) ) {
	define( 'GIVE_AUTHORIZE_PLUGIN_FILE', __FILE__ );
}
if ( ! defined( 'GIVE_AUTHORIZE_PLUGIN_DIR' ) ) {
	define( 'GIVE_AUTHORIZE_PLUGIN_DIR', dirname( __FILE__ ) );
}
if ( ! defined( 'GIVE_AUTHORIZE_PLUGIN_URL' ) ) {
	define( 'GIVE_AUTHORIZE_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
}
if ( ! defined( 'GIVE_AUTHORIZE_BASENAME' ) ) {
	define( 'GIVE_AUTHORIZE_BASENAME', plugin_basename( __FILE__ ) );
}

// Upgrades.
if ( file_exists( GIVE_AUTHORIZE_PLUGIN_DIR . '/includes/admin/give-authorize-upgrades.php' ) ) {
	include( GIVE_AUTHORIZE_PLUGIN_DIR . '/includes/admin/give-authorize-upgrades.php' );
}

/**
 * Authorize.net Licensing
 */
function give_add_authorize_net_licensing() {
	if ( class_exists( 'Give_License' ) ) {
		new Give_License( __FILE__, 'Authorize.net Gateway', GIVE_AUTHORIZE_VERSION, 'WordImpress' );
	}
}

add_action( 'plugins_loaded', 'give_add_authorize_net_licensing' );

/**
 * Class Give_Authorize
 */
final class Give_Authorize {

	/** Singleton *************************************************************/

	/**
	 * @var Give_Authorize The one true Give_Authorize
	 * @since 1.0
	 */
	private static $instance;

	/**
	 * Main Give_Authorize Instance
	 *
	 * Insures that only one instance of Give_Authorize exists in memory at any one
	 * time. Also prevents needing to define globals all over the place.
	 *
	 * @staticvar $instance array
	 *
	 * @return Give_Authorize object
	 */
	public static function instance() {
		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Give_Authorize ) ) {
			self::$instance = new Give_Authorize;
			self::$instance->includes();
			self::$instance->actions();

			// Admin only
			if ( is_admin() ) {

			}
		}

		return self::$instance;
	}

	/**
	 * Defines all the actions used throughout plugin
	 *
	 * @since 1.2
	 * @return void
	 */
	private function actions() {

		add_filter( 'give_payment_gateways', array( $this, 'register_gateway' ) );
		add_action( 'give_gateway_checkout_label', array( $this, 'customize_payment_label' ), 10, 2 );

	}

	/**
	 * Include all files
	 *
	 * @since 1.0.0
	 * @return void
	 */
	private function includes() {

		include_once GIVE_AUTHORIZE_PLUGIN_DIR . '/includes/admin/give-authorize-activation.php';

		if ( ! class_exists( 'Give' ) ) {
			return;
		}

		// Public includes.
		include_once GIVE_AUTHORIZE_PLUGIN_DIR . '/includes/class-authorize-payments.php';

		// Admin only includes.
		if ( is_admin() ) {
			include_once GIVE_AUTHORIZE_PLUGIN_DIR . '/includes/admin/give-authorize-settings.php';
		}

		// Class Instances.
		self::$instance->payments = new Give_Authorize_Payments();

	}

	/**
	 * Register Gateway
	 *
	 * Registers the gateway
	 *
	 * @param $gateways
	 *
	 * @return mixed
	 */
	function register_gateway( $gateways ) {
		// Format: ID => Name
		$gateways['authorize'] = array(
			'admin_label'    => __( 'Authorize.net', 'give-authorize-net' ),
			'checkout_label' => __( 'Credit Card', 'give-authorize-net' ),
		);

		return $gateways;
	}

	/**
	 * Customize Payment Label
	 *
	 * @param $label
	 * @param $gateway
	 *
	 * @return string $label
	 */
	public function customize_payment_label( $label, $gateway ) {

		if ( $gateway == 'authorizenet' ) {
			$label = __( 'Credit Card', 'give-authorize-net' );
		}

		return $label;
	}

}

/**
 * The main function responsible for returning the one true Give_Authorize
 * Instance to functions everywhere.
 *
 * Use this function like you would a global variable, except without needing
 * to declare the global.
 *
 * @since 1.1
 * @return object The one true Give_Form_Fields_Manager Instance
 */
function Give_Authorize() {

	return Give_Authorize::instance();
}

add_action( 'plugins_loaded', 'Give_Authorize' );
