<?php
/**
 * Plugin Name: Give - AmeriCloud Payments
 * Plugin URI: https://givewp.com/addons/americloud-gateway/
 * Description: Adds AmeriCloud Payments gateway to the available Give payment methods.
 * Version: 1.2.0
 * Author: AmeriCloud Solutions, Inc.
 * Author URI: http://www.americloud.net/payments
 */

// Exit if accessed directly.
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Define constants.
 *
 * Required minimum versions, paths, urls, etc.
 */
if ( ! defined( 'GIVE_AMERICLOUDPAYMENTS_VERSION' ) ) {
	define( 'GIVE_AMERICLOUDPAYMENTS_VERSION', '1.2.0' );
}
if ( ! defined( 'GIVE_AMERICLOUDPAYMENTS_MIN_GIVE_VER' ) ) {
	define( 'GIVE_AMERICLOUDPAYMENTS_MIN_GIVE_VER', '1.8' );
}
if ( ! defined( 'GIVE_AMERICLOUDPAYMENTS_MIN_PHP_VER' ) ) {
	define( 'GIVE_AMERICLOUDPAYMENTS_MIN_PHP_VER', '5.5.0' );
}
if ( ! defined( 'GIVE_AMERICLOUDPAYMENTS_PLUGIN_FILE' ) ) {
	define( 'GIVE_AMERICLOUDPAYMENTS_PLUGIN_FILE', __FILE__ );
}
if ( ! defined( 'GIVE_AMERICLOUDPAYMENTS_PLUGIN_DIR' ) ) {
	define( 'GIVE_AMERICLOUDPAYMENTS_PLUGIN_DIR', dirname( GIVE_AMERICLOUDPAYMENTS_PLUGIN_FILE ) );
}
if ( ! defined( 'GIVE_AMERICLOUDPAYMENTS_PLUGIN_URL' ) ) {
	define( 'GIVE_AMERICLOUDPAYMENTS_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
}
if ( ! defined( 'GIVE_AMERICLOUDPAYMENTS_BASENAME' ) ) {
	define( 'GIVE_AMERICLOUDPAYMENTS_BASENAME', plugin_basename( __FILE__ ) );
}

require_once "includes/initialize.php";


/**
 * AmeriCloud Licensing
 */
function give_add_americloud_licensing() {
	if ( class_exists( 'Give_License' ) ) {
		new Give_License( __FILE__, 'AmeriCloud Payments', GIVE_AMERICLOUDPAYMENTS_VERSION, 'WordImpress' );
	}
}

add_action( 'plugins_loaded', 'give_add_americloud_licensing' );
