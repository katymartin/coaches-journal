jQuery(function ($) {
    $(document).ready(function () {
        $('#give-refund').on('click', function (e) {
            if(confirm("Refund this donation?") === false) {
                e.preventDefault();
                return false;
            }
        });
    });
});