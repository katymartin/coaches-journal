jQuery(function ($) {
    $(document).ready(function () {
        $("#give-donation-level-button-wrap .give-donation-level-btn").each(function () {
            $(this).click(function () {
                var spantext = $('.agree-field-message .amount');
                if(spantext.length > 0){
                    spantext.html($(this).html());
                }
            })
        });

        $(document).ajaxComplete(function () {
            if ($('#give_ec').length > 0) {
                $('#bank-account-type').on('change', function () {

                    console.log(this.value);

                    var inputWrap = $('#give-company-name-wrap'),
                        input = inputWrap.find('input[name="company_name"]');
                    if (this.value == 1) {
                        input.attr('required', 'true');
                        inputWrap.show();
                    }
                    else {
                        inputWrap.hide();
                        input.removeAttr('required');
                    }
                });

                $('#give-amount').on('input', function () {
                    $('span.amount').html('$' + this.value);
                });
            }
        });
    });
});