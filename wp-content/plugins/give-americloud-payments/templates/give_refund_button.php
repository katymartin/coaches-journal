<div id="major-publishing-actions">
    <div id="publishing-action">
        <a href="<?php echo esc_url( add_query_arg( array(
            'give-action' => 'refund',
            'purchase_id' => $payment_id,
        ) ) ); ?>" id="give-refund" class="button-secondary right"><?php esc_html_e('Refund', 'give-americloud'); ?></a>
    </div>
    <div class="clear"></div>
</div>