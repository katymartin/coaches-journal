<fieldset id="give_ec" class="americloud-fields-wrap give-do-validate">

    <?php if (is_ssl()) : ?>
        <div id="give_secure_site_wrapper">
            <span class="give-icon padlock"></span>
            <span><?php esc_html_e('This is a secure SSL encrypted payment.', 'give-americloud'); ?></span>
        </div>
    <?php endif; ?>

    <legend><?php esc_html_e('Electronic Check Info', 'give-americloud'); ?></legend>

    <p id="give-routing-number-wrap" class="form-row form-row-one-thirds">
        <label for="routing-number" class="give-label">
            <?php esc_html_e('Routing Number', 'give-americloud'); ?>
            <span class="give-required-indicator">*</span>
        </label>
        <input type="tel" autocomplete="off" name="routing_number" id="routing-number"
               required class="routing-number give-input required"
               placeholder="<?php esc_attr_e('Routing Number', 'give-americloud'); ?>"/>
    </p>

    <p id="give-account-number-wrap" class="form-row form-row-one-thirds">
        <label for="account-number" class="give-label">
            <?php esc_html_e('Account Number', 'give-americloud'); ?>
            <span class="give-required-indicator">*</span>
        </label>
        <input type="tel" autocomplete="off" name="account_number" id="account-number"
               required class="account-number give-input required"
               placeholder="<?php esc_attr_e('Account Number', 'give-americloud'); ?>"/>
    </p>

    <p id="give-bank-account-type-wrap" class="form-row form-row-first form-row-responsive">
        <label for="bank-account-type" class="give-label">
            <?php esc_html_e('Choose bank account type', 'give-americloud'); ?>
            <span class="give-required-indicator">*</span>
        </label>
        <select name="bank_account_type" id="bank-account-type" class="give-input required" required>
            <option value="0"><?php esc_html_e('Personal', 'give-americloud'); ?></option>
            <option value="1"><?php esc_html_e('Business', 'give-americloud'); ?></option>
        </select>
    </p>

    <p id="give-company-name-wrap" class="form-row form-row-last form-row-responsive">
        <label for="company-name" class="give-label">
            <?php esc_html_e('Company Name', 'give-americloud'); ?>
            <span class="give-required-indicator">*</span>
        </label>
        <input type="text" autocomplete="off" name="company_name" id="company-name"
               class="company-name give-input"
               placeholder="<?php esc_attr_e('Company Name', 'give-americloud'); ?>"/>
    </p>

    <p id="give-agree-field-wrap" class="form-row form-row-wide">
        <?php $sitename = '<b>' . get_bloginfo('name') . '</b>';
        $default_amount = give_format_amount(give_get_default_form_amount($form_id)); ?>
    <p class="agree-field-message">
        <?php echo __('By providing my bank details, I authorize ' . $sitename . ' to use information above to initiate an electronic fund transfer from my account or to process the payment as a check transaction or bank drawn draft from my account for the amount of <b><span class="amount">$'. $default_amount .'</span></b>. 
        If my payment is returned due to insufficient funds, I authorize ' . $sitename . ' to make a one-time electronic funds transfer or to use a bank draft drawn from my account to collect a fee as allowed by state law.'); ?>
    </p>
    <input type="hidden" name="agree_field" id="agree-field" class="agree-field give-input" value="1"/>
    </p>
</fieldset>
