<?php
/**
* Fires while rendering credit card info form, before the fields.
*
* @since 1.0
*
* @param int $form_id The form ID.
*/
do_action( 'give_before_cc_fields', $form_id );
?>
<fieldset id="give_cc_fields-<?php echo $form_id ?>" class="americloud-fields-wrap give-do-validate">
	<legend><?php echo apply_filters( 'give_credit_card_fieldset_heading', esc_html__( 'Credit Card Info', 'give-americloud' ) ); ?></legend>
	<?php if ( is_ssl() ) : ?>
		<div id="give_secure_site_wrapper-<?php echo $form_id ?>">
			<span class="give-icon padlock"></span>
			<span><?php esc_html_e( 'This is a secure SSL encrypted payment.', 'give-americloud' ); ?></span>
		</div>
	<?php endif; ?>

	<p id="give-card-name-wrap-<?php echo $form_id ?>" class="form-row form-row-wide form-row-responsive">
		<label for="card_name-<?php echo $form_id ?>" class="give-label">
			<?php esc_html_e( 'Name on the Card', 'give-americloud' ); ?>
			<span class="give-required-indicator">*</span>
			<span class="give-tooltip give-icon give-icon-question"
				  data-tooltip="<?php esc_attr_e( 'The name printed on the front of your credit card.', 'give-americloud' ); ?>"></span>
		</label>

		<input type="text" autocomplete="off" name="card_name" id="card_name-<?php echo $form_id ?>"
			   class="card-name give-input required" placeholder="<?php esc_attr_e( 'Card name', 'give-americloud' ); ?>"
			   required aria-required="true"/>
	</p>

	<p id="give-card-number-wrap-<?php echo $form_id ?>" class="form-row form-row-wide form-row-responsive">
		<label for="card_number-<?php echo $form_id ?>" class="give-label">
			<?php esc_html_e( 'Card Number', 'give-americloud' ); ?>
			<span class="give-required-indicator">*</span>
			<span class="give-tooltip give-icon give-icon-question"
				  data-tooltip="<?php esc_attr_e( 'The (typically) 16 digits on the front of your credit card.', 'give-americloud' ); ?>"></span>
			<span class="card-type"></span>
		</label>

		<input type="tel" autocomplete="off" name="card_number" id="card_number-<?php echo $form_id ?>"
			   class="card-number give-input required" placeholder="<?php esc_attr_e( 'Card number', 'give-americloud' ); ?>"
			   required aria-required="true"/>
	</p>

	<?php
	/**
	 * Fires while rendering credit card info form, before expiration fields.
	 *
	 * @since 1.0
	 *
	 * @param int $form_id The form ID.
	 */
	do_action( 'give_before_cc_expiration' );
	?>
	<p class="card-expiration form-row form-row-first form-row-responsive">
		<label for="card_expiry-<?php echo $form_id ?>" class="give-label">
			<?php esc_html_e( 'Expiration', 'give-americloud' ); ?>
			<span class="give-required-indicator">*</span>
			<span class="give-tooltip give-icon give-icon-question"
				  data-tooltip="<?php esc_attr_e( 'The date your credit card expires, typically on the front of the card.', 'give-americloud' ); ?>"></span>
		</label>

		<input type="hidden" id="card_exp_month-<?php echo $form_id ?>" name="card_exp_month"
			   class="card-expiry-month"/>
		<input type="hidden" id="card_exp_year-<?php echo $form_id ?>" name="card_exp_year"
			   class="card-expiry-year"/>

		<input type="tel" autocomplete="off" name="card_expiry" id="card_expiry-<?php echo $form_id ?>"
			   class="card-expiry give-input required" placeholder="<?php esc_attr_e( 'MM / YY', 'give-americloud' ); ?>"
			   required aria-required="true" />
	</p>

	<?php
	/**
	 * Fires while rendering credit card info form, after expiration fields.
	 *
	 * @since 1.0
	 *
	 * @param int $form_id The form ID.
	 */
	do_action( 'give_after_cc_expiration', $form_id );
	?>

	<p id="give-card-cvc-wrap-<?php echo $form_id ?>" class="form-row form-row-last form-row-responsive">
		<label for="card_cvc-<?php echo $form_id ?>" class="give-label">
			<?php esc_html_e( 'CVC', 'give-americloud' ); ?>
			<span class="give-required-indicator">*</span>
			<span class="give-tooltip give-icon give-icon-question"
				  data-tooltip="<?php esc_attr_e( 'The 3 digit (back) or 4 digit (front) value on your card.', 'give-americloud' ); ?>"></span>
		</label>

		<input type="tel" size="4" autocomplete="off" name="card_cvc" id="card_cvc-<?php echo $form_id ?>"
			   class="card-cvc give-input required" placeholder="<?php esc_attr_e( 'Security code', 'give-americloud' ); ?>"
			   required aria-required="true"/>
	</p>
</fieldset>
