<?php
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Class Give_AmeriCloudPayments_UI.
 */
if (!class_exists('Give_AmeriCloudPayments_UI')):

    class Give_AmeriCloudPayments_UI
    {

        /**
         * Give_AmeriCloudPayments_Gateway constructor.
         */
        function __construct()
        {
            add_action('give_americloudpayments_cc_form', array($this, 'give_americloudpayments_cc_form'));
            add_action('give_americloudpayments_ach_cc_form', array($this, 'give_americloudpayments_ach_form'));
        }

        /**
         * Show forms for front-end
         */
        public function give_americloudpayments_cc_form($form_id)
        {
            $plate_engine = new League\Plates\Engine(GIVE_AMERICLOUDPAYMENTS_PLUGIN_DIR . "/templates");
            print $plate_engine->render('give_cc_form', compact('form_id') );

            $this->give_collect_billing($form_id);
        }

        public function give_americloudpayments_ach_form($form_id)
        {
            $plate_engine = new League\Plates\Engine(GIVE_AMERICLOUDPAYMENTS_PLUGIN_DIR . "/templates");
            print $plate_engine->render('give_ach_form', compact('form_id'));
            $this->give_collect_billing($form_id);
        }

        public function give_collect_billing($form_id)
        {
            $billing_fields_enabled = give_get_option('americloud_collect_billing');

            //Remove Address Fields if user has option enabled
            if (!$billing_fields_enabled) {
                remove_action('give_after_cc_fields', 'give_default_cc_address_fields');
            } else {

                do_action('give_after_cc_fields', $form_id);
            }
        }

    }

    new Give_AmeriCloudPayments_UI();
endif;