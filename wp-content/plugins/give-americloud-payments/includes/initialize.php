<?php
if (!class_exists('Give_AmeriCloudPayments')) :

    /**
     * Class Give_AmeriCloudPayments.
     */
    class Give_AmeriCloudPayments
    {

        /**
         * @var Singleton The reference the *Singleton* instance of this class.
         */
        private static $instance;

        /**
         * @var Give_AmeriCloudPayments_Upgrades.
         */
        public $upgrades;

        /**
         * Notices (array)
         * @var array
         */
        public $notices = array();

        /**
         * Returns the *Singleton* instance of this class.
         *
         * @return Singleton The *Singleton* instance.
         */
        public static function get_instance()
        {
            if (null === self::$instance) {
                self::$instance = new self();
            }

            return self::$instance;
        }

        /**
         * Private clone method to prevent cloning of the instance of the
         * *Singleton* instance.
         *
         * @return void
         */
        private function __clone()
        {
        }

        /**
         * Give_americloudpayments constructor.
         *
         * Protected constructor to prevent creating a new instance of the
         * *Singleton* via the `new` operator from outside of this class.
         */
        protected function __construct()
        {
            add_action('admin_init', array($this, 'check_environment'));
            add_action('admin_notices', array($this, 'admin_notices'), 15);
            add_action('plugins_loaded', array($this, 'init'));
            add_action('admin_enqueue_scripts', array($this, 'confirm_refund'));
        }


        /**
         * Init the plugin after plugins_loaded so environment variables are set.
         */
        public function init()
        {

            // Don't hook anything else in the plugin if we're in an incompatible environment.
            if (self::get_environment_warning()) {
                return;
            }

            add_filter('give_payment_gateways', array($this, 'register_gateway'));

            $this->includes();

        }

        public function confirm_refund($hook)
        {
//            if ('edit.php' != $hook) {
//                return;
//            }
            wp_enqueue_script('my_custom_script', GIVE_AMERICLOUDPAYMENTS_PLUGIN_URL . '/assets/js/confirm_refund.js' );
        }

        /**
         * Allow this class and other classes to add notices.
         *
         * @param $slug
         * @param $class
         * @param $message
         */
        public function add_admin_notice($slug, $class, $message)
        {
            $this->notices[$slug] = array(
                'class' => $class,
                'message' => $message
            );
        }

        /**
         * Display admin notices.
         */
        public function admin_notices()
        {

            $allowed_tags = array(
                'a' => array(
                    'href' => array(),
                    'title' => array()
                ),
                'br' => array(),
                'em' => array(),
                'strong' => array(),
            );

            foreach ((array)$this->notices as $notice_key => $notice) {
                echo "<div class='" . esc_attr($notice['class']) . "'><p>";
                echo wp_kses($notice['message'], $allowed_tags);
                echo "</p></div>";
            }

        }

        /**
         * The primary sanity check, automatically disable the plugin on activation if it doesn't
         * meet minimum requirements.
         *
         * Based on http://wptavern.com/how-to-prevent-wordpress-plugins-from-activating-on-sites-with-incompatible-hosting-environments
         */
        public static function activation_check()
        {
            $environment_warning = self::get_environment_warning(true);
            if ($environment_warning) {
                deactivate_plugins(plugin_basename(__FILE__));
                wp_die($environment_warning);
            }
        }

        /**
         * Check the server environment.
         *
         * The backup sanity check, in case the plugin is activated in a weird way,
         * or the environment changes after activation.
         */
        public function check_environment()
        {

            $environment_warning = self::get_environment_warning();
            if ($environment_warning && is_plugin_active(plugin_basename(__FILE__))) {
                deactivate_plugins(plugin_basename(__FILE__));
                $this->add_admin_notice('bad_environment', 'error', $environment_warning);
                if (isset($_GET['activate'])) {
                    unset($_GET['activate']);
                }
            }

            // Check for if give plugin activate or not.
            $is_give_active = defined('GIVE_PLUGIN_BASENAME') ? is_plugin_active(GIVE_PLUGIN_BASENAME) : false;
            //Check to see if Give is activated, if it isn't deactivate and show a banner.
            if (is_admin() && current_user_can('activate_plugins') && !$is_give_active) {

                $this->add_admin_notice('prompt_connect', 'error', sprintf(__('<strong>Activation Error:</strong> You must have the <a href="%s" target="_blank">Give</a> plugin installed and activated for AmeriCloud to activate.', 'give-americloudpayments'), 'https://givewp.com'));

                //Don't let this plugin activate
                deactivate_plugins(plugin_basename(__FILE__));

                if (isset($_GET['activate'])) {
                    unset($_GET['activate']);
                }

                return false;

            }

            //Check min Give version.
            if (defined('GIVE_AMERICLOUDPAYMENTS_MIN_GIVE_VER') && version_compare(GIVE_VERSION, GIVE_AMERICLOUDPAYMENTS_MIN_GIVE_VER, '<')) {

                $this->add_admin_notice('prompt_connect', 'error', sprintf(__('<strong>Activation Error:</strong> You must have the <a href="%s" target="_blank">Give</a> core version %s+ for the Give AmeriCloud add-on to activate.', 'give-americloudpayments'), 'https://givewp.com', GIVE_AMERICLOUDPAYMENTS_MIN_GIVE_VER));

                //Don't let this plugin activate.
                deactivate_plugins(plugin_basename(__FILE__));

                if (isset($_GET['activate'])) {
                    unset($_GET['activate']);
                }

                return false;

            }

            $give_americloudpayments = new Give_AmeriCloudPayments_Gateway();
            $give_americloudpayments_ach = new Give_AmeriCloudPayments_ACH();
            $is_admin_keys = $give_americloudpayments->check_admin_keys();
            $is_admin_keys_ach = $give_americloudpayments_ach->check_admin_keys();

            //Show prompt needing to connect API key if gateway is active.
            if ((!$is_admin_keys) && give_is_gateway_active('americloudpayments') || ((!$is_admin_keys_ach) && give_is_gateway_active('americloudpayments'))) {
                var_dump($is_admin_keys);
                $this->add_admin_notice('prompt_connect', 'notice notice-warning', sprintf(__('The AmeriCloud payment gateway is almost ready. To get started, <a href="%s">set your AmeriCloud account keys</a>.', 'give-americloudpayments'), admin_url('edit.php?post_type=give_forms&page=give-settings&tab=gateways&section=americloud-settings')));
            }

        }

        /**
         * Environment warnings.
         *
         * Checks the environment for compatibility problems.
         * Returns a string with the first incompatibility found or false if the environment has no problems.
         *
         * @param bool $during_activation
         *
         * @return bool|mixed|string
         */
        public static function get_environment_warning($during_activation = false)
        {

            if (version_compare(phpversion(), GIVE_AMERICLOUDPAYMENTS_MIN_PHP_VER, '<')) {
                if ($during_activation) {
                    $message = __('The plugin could not be activated. The minimum PHP version required for this plugin is %1$s. You are running %2$s. Please contact your web host to upgrade your server\'s PHP version.', 'give-americloudpayments');
                } else {
                    $message = __('The plugin has been deactivated. The minimum PHP version required for this plugin is %1$s. You are running %2$s.', 'give-americloudpayments');
                }

                return sprintf($message, GIVE_AMERICLOUDPAYMENTS_MIN_PHP_VER, phpversion());
            }

            if (!function_exists('curl_init')) {

                if ($during_activation) {
                    return __('The plugin could not be activated. cURL is not installed. Please contact your web host to install cURL.', 'give-americloudpayments');
                }

                return __('The plugin has been deactivated. cURL is not installed. Please contact your web host to install cURL.', 'give-americloudpayments');
            }

            return false;
        }

        /**
         * Give AmeriCloudPayments Includes.
         */
        private function includes()
        {

            // Checks if Give is installed.
            if (!class_exists('Give')) {
                return false;
            }

            require_once GIVE_AMERICLOUDPAYMENTS_PLUGIN_DIR . '/vendor/autoload.php';

            if (is_admin()) {
                include(GIVE_AMERICLOUDPAYMENTS_PLUGIN_DIR . '/includes/admin/admin-americloud.php');
            }
            require_once GIVE_AMERICLOUDPAYMENTS_PLUGIN_DIR . '/includes/give-americloud-ui.php';
            require_once GIVE_AMERICLOUDPAYMENTS_PLUGIN_DIR . '/includes/give-americloud-refund.php';
            require_once GIVE_AMERICLOUDPAYMENTS_PLUGIN_DIR . '/includes/give-americloud-gateway.php';
            require_once GIVE_AMERICLOUDPAYMENTS_PLUGIN_DIR . '/includes/give-americloud-ach.php';
        }

        /**
         * Register the AmeriCloudPayments payment gateways.
         *
         * @access      public
         * @since       1.0
         * @return      array
         */
        public function register_gateway($gateways)
        {

            // Format: ID => Name
            $gateways['americloudpayments']     = array(
                'admin_label'    => esc_html__( 'AmeriCloud Payments - Credit Card', 'give-americloudpayments' ),
				        'checkout_label' => esc_html__( 'Credit Card', 'give-americloudpayments' ),
			);

			$gateways['americloudpayments_ach'] = array(
				'admin_label'    => esc_html__( 'AmeriCloud Payments - Electronic Check (ACH)', 'give-americloudpayments' ),
				'checkout_label' => esc_html__( 'Electronic Check', 'give-americloudpayments' ),
			);

            return $gateways;
        }
    }

    $GLOBALS['give_americloudpayments'] = Give_AmeriCloudPayments::get_instance();
    register_activation_hook(__FILE__, array('Give_AmeriCloudPayments', 'activation_check'));

endif; // End if class_exists check.
