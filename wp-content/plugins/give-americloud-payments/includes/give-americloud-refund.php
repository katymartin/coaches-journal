<?php

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Class Give_AmeriCloudPayments_Gateway.
 */
if (!class_exists('Give_AmeriCloudPayments_RefundGateway')):

    class Give_AmeriCloudPayments_RefundGateway
    {
        /**
         * Give_AmeriCloudPayments_RefundGateway constructor.
         */
        function __construct()
        {
            add_action('give_refund', array($this, 'give_process_refund'));
            add_action('give_view_order_details_totals_after', array($this, 'give_button_refund'));
        }


        /**
         * Process purchase refund.
         *
         * @param  array $data
         * @return void
         */
        public function give_process_refund($data)
        {
            $purchase_id = absint($data['purchase_id']);

            if (empty($purchase_id)) {
                return;
            }

            $txn_id = give_get_payment_transaction_id($purchase_id);

            if (empty($txn_id)) {
                return;
            }

            if (!current_user_can('edit_give_payments', $purchase_id)) {
                wp_die(esc_html__('You do not have permission to edit payments.', 'give'), esc_html__('Error', 'give'), array('response' => 403));
            }

            $type = trim(give_get_option('americloud_account_type'));
            $merchantId = trim(give_get_option('americloud_merchant_id'));
            $userId = trim(give_get_option('americloud_user_id'));
            $pin = trim(give_get_option('americloud_pin'));

            $PaymentProcessor = new \markroland\Converge\ConvergeApi(
                $merchantId,
                $userId,
                $pin,
                $type
            );

            $response = $PaymentProcessor->ccvoid([
                'ssl_txn_id' => $txn_id,
            ]);

            // Check response
            if (!empty($response['errorCode'])) {

                $response = $PaymentProcessor->ccreturn([
                    'ssl_txn_id' => $txn_id,
                ]);

                if (!empty($response['errorMessage'])) {
                    $error = 'Refund error.';
                    $error .= ' ' . $response['errorMessage'];
                    wp_die(esc_html__($error, 'give'), esc_html__('Error', 'give'));
                }
            }

            give_update_payment_status($purchase_id, 'refunded');
			      give_insert_payment_note( $payment_id, esc_html__( 'Refunded.', 'give-americloudpayments' ) );

            give_set_payment_transaction_id($purchase_id, $response['ssl_txn_id']);
            wp_redirect('/wp-admin/edit.php?post_type=give_forms&page=give-payment-history');
            exit;
        }

        public function give_button_refund($payment_id)
        {
            $payment = new Give_Payment($payment_id);
            $status = strtolower(give_get_payment_status($payment));
            $gateway = strtolower(give_get_payment_gateway($payment_id));

            if ($status != 'refunded' && $gateway == 'americloudpayments') {
                $plate_engine = new League\Plates\Engine(GIVE_AMERICLOUDPAYMENTS_PLUGIN_DIR . "/templates");
                print $plate_engine->render('give_refund_button', compact('payment_id'));
            }
        }
    }

    new Give_AmeriCloudPayments_RefundGateway();

endif;
