<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Give - Americloud Payments admin settings
 */
function give_americloud_add_settings( $settings ) {
	$americloud_settings = array(
		array(
			'name' => esc_html__( 'AmeriCloud Payments', 'give-americloud' ),
			'desc' => '<hr>',
			'id'   => 'give_title_americloud',
			'type' => 'give_title'
		),
		array(
			'name'    => esc_html__( 'Account Type', 'give-americloud' ),
			'desc'    => esc_html__( 'Please choose demo or production account.', 'give-americloud' ),
			'id'      => 'americloud_account_type',
			'type'    => 'select',
			'options' => array(
				'FALSE' => esc_html__( 'Demo', 'give-americloud' ),
				'TRUE'  => esc_html__( 'Production', 'give-americloud' ),
			),
		),

		array(
			'name' => esc_html__( 'Account ID', 'give-americloud' ),
			'desc' => esc_html__( 'Enter your Account ID, provided by AmeriCloud Payments.', 'give-americloud' ),
			'id'   => 'americloud_merchant_id',
			'type' => 'text',
		),
		array(
			'name' => esc_html__( 'User ID', 'give-americloud ' ),
			'desc' => esc_html__( 'Enter your user ID, provided by AmeriCloud Payments.', 'give-americloud' ),
			'id'   => 'americloud_user_id',
			'type' => 'text'
		),
		array(
			'name' => esc_html__( 'PIN', 'give-americloud' ),
			'desc' => esc_html__( 'Enter your PIN, provided by AmeriCloud Payments.', 'give-americloud' ),
			'id'   => 'americloud_pin',
			'type' => 'password'
		),

		array(
			'name' => esc_html__( 'Collect Billing Details', 'give-americloud' ),
			'desc' => esc_html__( 'This option will enable the billing details section for AmeriCloud Payments which requires the donor\'s address to complete the donation. These fields are not required by AmeriCloud Payments to process the transaction, but you may have the need to collect the data.', 'give-americloud' ),
			'id'   => 'americloud_collect_billing',
			'type' => 'checkbox'
		),

		array(
			'name' => esc_html__( 'Enable Recurring Donations', 'give-americloud' ),
			'desc' => esc_html__( 'This option will allow your donors to create a monthly recurring donation. By default, recurring donations start on the first day of the next month. Note: You DO NOT need to use Give - Recurring Donations add-on to use this functionality. If you have it, please disable it.', 'give-americloud' ),
			'id'   => 'americloud_enable_recurring',
			'type' => 'checkbox'
		),
	);

	return array_merge( $settings, $americloud_settings );

}

add_filter( 'give_settings_gateways', 'give_americloud_add_settings' );

/**
 * Plugins row action links
 */
function give_americloud_plugin_action_links( $actions ) {
	$new_actions = array(
		'settings' => sprintf(
			'<a href="%1$s">%2$s</a>',
			admin_url( 'edit.php?post_type=give_forms&page=give-settings&tab=gateways&section=americloud-payments' ),
			esc_html__( 'Settings', 'give-americloud' )
		),
	);

	return array_merge( $new_actions, $actions );
}

add_filter( 'plugin_action_links_' . GIVE_AMERICLOUDPAYMENTS_BASENAME, 'give_americloud_plugin_action_links' );


/**
 * Plugin row meta links
 *
 * @since 1.0
 *
 * @param array $plugin_meta An array of the plugin's metadata.
 * @param string $plugin_file Path to the plugin file, relative to the plugins directory.
 *
 * @return array
 */
function give_americloud_plugin_row_meta( $plugin_meta, $plugin_file ) {
	if ( $plugin_file != GIVE_AMERICLOUDPAYMENTS_BASENAME ) {
		return $plugin_meta;
	}

	$new_meta_links = array(
		sprintf(
			'<a href="%1$s" target="_blank">%2$s</a>',
			esc_url( add_query_arg( array(
					'utm_source'   => 'plugins-page',
					'utm_medium'   => 'plugin-row',
					'utm_campaign' => 'admin',
				), 'http://docs.givewp.com/addon-americloud' )
			),
			esc_html__( 'Documentation', 'give-americloud' )
		),
		sprintf(
			'<a href="%1$s" target="_blank">%2$s</a>',
			esc_url( add_query_arg( array(
					'utm_source'   => 'plugins-page',
					'utm_medium'   => 'plugin-row',
					'utm_campaign' => 'admin',
				), 'https://givewp.com/addons/' )
			),
			esc_html__( 'Add-ons', 'give-americloud' )
		),
	);

	return array_merge( $plugin_meta, $new_meta_links );
}

add_filter( 'plugin_row_meta', 'give_americloud_plugin_row_meta', 10, 2 );


/**
 * Give AmeriCloud activation banner.
 *
 * Includes and initializes Give activation banner class.
 *
 * @since 1.0
 */
function give_americloud_activation_banner() {

	// Check for activation banner inclusion.
	if (
		! class_exists( 'Give_Addon_Activation_Banner' )
		&& file_exists( GIVE_PLUGIN_DIR . 'includes/admin/class-addon-activation-banner.php' )
	) {
		include GIVE_PLUGIN_DIR . 'includes/admin/class-addon-activation-banner.php';
	}

	// Initialize activation welcome banner.
	if ( class_exists( 'Give_Addon_Activation_Banner' ) ) {

		//Only runs on admin
		$args = array(
			'file'              => __FILE__,
			'name'              => esc_html__( 'AmeriCloud Payments', 'give-americloud' ),
			'version'           => GIVE_AMERICLOUDPAYMENTS_VERSION,
			'settings_url'      => admin_url( 'edit.php?post_type=give_forms&page=give-settings&tab=gateways&section=americloud-payments' ),
			'documentation_url' => 'http://docs.givewp.com/addon-americloud',
			'support_url'       => 'https://givewp.com/support/',
			'testing'           => false
		);

		new Give_Addon_Activation_Banner( $args );

	}

	return true;

}

add_action( 'admin_init', 'give_americloud_activation_banner' );
