<?php
/*
Author: Amjad Ali
AmeriCloud Payments
*/
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * Class Give_AmeriCloudPayments_Gateway.
 */
if ( ! class_exists( 'Give_AmeriCloudPayments_Gateway' ) ):

	class Give_AmeriCloudPayments_Gateway {
		/**
		 * Give_AmeriCloudPayments_Gateway constructor.
		 */
		function __construct() {
			add_action( 'give_gateway_americloudpayments', array( $this, 'give_process_payment' ) );
			add_action('wp_enqueue_scripts', array($this, 'load_scripts'));

			add_action( 'wp_enqueue_scripts', 'give_load_scripts' );
		}


		/**
		 * Load Scripts for AmeriCloud
		 */
		function load_scripts(){
			wp_register_script('give-americloud', GIVE_AMERICLOUDPAYMENTS_PLUGIN_URL . 'assets/js/give-americloud.js', array('jquery'), '', true);
			wp_enqueue_script('give-americloud');

			wp_register_style('give-americloud', GIVE_AMERICLOUDPAYMENTS_PLUGIN_URL . 'assets/css/give-americloud.css', array(), '0.1');
			wp_enqueue_style('give-americloud');

		}

		/**
		 * Process user submission
		 *
		 * @param $payment_data
		 */
		public function give_process_payment( $payment_data ) {

			$recurring_enabled = give_get_option('americloud_enable_recurring');

			// get recurring check box
			if ($recurring_enabled) {
					$recurring_donation = $_POST['recurring-donation'];
			}

			// Validate nonce.
			give_validate_nonce( $payment_data['gateway_nonce'], 'give-gateway' );
			$this->verify_cc_post_data();

			if( ! empty( give_get_errors()) ) {
				give_send_back_to_checkout( "?payment-mode={$payment_data['gateway']}&form_id={$payment_data['post_data']['give-form-id']}" );
			}

			$payment_id = $this->give_create_payment( $payment_data );

			$data       = [];

			// Check payment.
			if ( ! empty( $payment_id ) ) {

				$data['fname']       = empty( $payment_data['user_info']['first_name'] ) ? '' : $payment_data['user_info']['first_name'];
				$data['lname']       = empty( $payment_data['user_info']['last_name'] ) ? '' : $payment_data['user_info']['last_name'];
				$data['price']       = empty( $payment_data['price'] ) ? '' : $payment_data['price'];
				$data['card_number'] = empty( $payment_data['card_info']['card_number'] ) ? '' : $payment_data['card_info']['card_number'];
				$data['cvv2cvc2']    = empty( $payment_data['card_info']['card_cvc'] ) ? '' : $payment_data['card_info']['card_cvc'];
				$data['exp_date']    = empty( $payment_data['post_data']['card_expiry'] ) ? '' : $payment_data['post_data']['card_expiry'];

				$data['card_address']    = empty( $payment_data['post_data']['card_address'] ) ? '' : $payment_data['post_data']['card_address'];
				$data['card_address_2']  = empty( $payment_data['post_data']['card_address_2'] ) ? '' : $payment_data['post_data']['card_address_2'];
				$data['card_city']       = empty( $payment_data['post_data']['card_city'] ) ? '' : $payment_data['post_data']['card_city'];
				$data['card_zip']        = empty( $payment_data['post_data']['card_zip'] ) ? '' : $payment_data['post_data']['card_zip'];
				$data['card_state']      = empty( $payment_data['post_data']['card_state'] ) ? '' : $payment_data['post_data']['card_state'];
				$data['billing_country'] = empty( $payment_data['post_data']['billing_country'] ) ? '' : $payment_data['post_data']['billing_country'];

				$data['user_email']      = empty( $payment_data['post_data']['give_email'] ) ? '' : $payment_data['post_data']['give_email'];
				$data['form_name']       = empty( $payment_data['post_data']['give-form-title'] ) ? '' : $payment_data['post_data']['give-form-title'];

				if ($recurring_enabled) {
						$data['recurring_donation'] = $recurring_donation;
				}

				$monthYear = explode("/",$data['exp_date']);

				if (strlen(str_replace(' ','',$monthYear[1])) == 4) {
					//$monthYear[1] = $monthYear[1] - 2000;
					$monthYear[1] = substr($monthYear[1], -2);
					$data['exp_date'] = str_replace(' ','',$monthYear[0].$monthYear[1]);
				}

				// Call the Payment API
				$response = $this->give_process_payment_converge( $data );

				//  echo "<pre>";
				//  print_r($response);
				//  echo "</pre>";

				if ( $response['ssl_result_message'] == 'APPROVAL' ) {
						give_set_payment_transaction_id( $payment_id, $response['ssl_txn_id'] );
						give_insert_payment_note( $payment_id, 'Card No. ' . $response['ssl_card_number']);
						give_insert_payment_note( $payment_id, 'Result: ' . $response['ssl_result_message']);
						give_insert_payment_note( $payment_id, 'Approval Code: ' . $response['ssl_approval_code']);
						give_update_payment_status( $payment_id, 'publish' ); // Completed
				}
				// recurring response
				else if ( $response['ssl_result_message'] == 'SUCCESS' ) {
						give_set_payment_transaction_id( $payment_id, $response['ssl_recurring_id'] );
						give_insert_payment_note( $payment_id, 'Card No.: ' . $response['ssl_card_number']);
						give_insert_payment_note( $payment_id, 'Card Type: ' . $response['ssl_card_short_description']);
						give_insert_payment_note( $payment_id, 'Next Payment Date: ' . $response['ssl_next_payment_date']);
						give_update_payment_status( $payment_id, 'pending' ); // Pending
				}
				else if ( isset( $response['errorCode'] ) ) {
						give_set_payment_transaction_id( $payment_id, 'Error' );
						give_insert_payment_note( $payment_id, 'Error Code: ' . $response['errorCode']);
						give_insert_payment_note( $payment_id, 'Error Name: ' . $response['errorName']);
						give_insert_payment_note( $payment_id, 'Error Message: ' . $response['errorMessage']);
						give_update_payment_status( $payment_id, 'failed' ); // Failed
						session_start();
						$_SESSION['errorName'] = $response['errorName'];
			  }
				else {
						give_set_payment_transaction_id( $payment_id, 'Error' );
						give_insert_payment_note( $payment_id, 'Error: ' . $response['errorName']);
						give_update_payment_status( $payment_id, 'failed' ); // Failed
						session_start();
						$_SESSION['errorName'] = $response['errorMessage'];
				}

				} else {
					// Record the error.
					give_record_gateway_error(
						esc_html__( 'Payment Error', 'give' ),
						sprintf(
						/* translators: %s: payment data */
							esc_html__( 'Payment creation failed before sending donor to payment gateway. Payment data: %s', 'give' ),
							json_encode( $payment_data )
						),
						$payment_id
					);

					// Problems? Send back.
					give_send_back_to_checkout( '?payment-mode=' . $payment_data['post_data']['give-gateway'] );
				}

			// Redirect after successful donation
			give_send_to_success_page();

			session_destroy();
		}

		/**
		 * Create Donate object for GIVE
		 *
		 * @param $payment_data
		 *
		 * @return bool|int
		 */
		private function give_create_payment( $payment_data ) {
			$gateway = 'americloudpayments';

			// Collect payment data.
			$insert_payment_data = array(
				'price'           => $payment_data['price'],
				'give_form_title' => $payment_data['post_data']['give-form-title'],
				'give_form_id'    => intval( $payment_data['post_data']['give-form-id'] ),
				'give_price_id'   => isset( $payment_data['post_data']['give-price-id'] ) ? $payment_data['post_data']['give-price-id'] : '',
				'date'            => $payment_data['date'],
				'user_email'      => $payment_data['user_email'],
				'purchase_key'    => $payment_data['purchase_key'],
				'currency'        => give_get_currency(),
				'user_info'       => $payment_data['user_info'],
				'status'          => 'pending',
				'gateway'         => $gateway
			);

			return give_insert_payment( $insert_payment_data );
		}

		/**
		 * Process payment to API
		 *
		 * @param $data
		 *
		 * @internal param $live_mode
		 */
		public function give_process_payment_converge( $data ) {
			$type       = trim( give_get_option( 'americloud_account_type' ) );
			$merchantId = trim( give_get_option( 'americloud_merchant_id' ) );
			$userId     = trim( give_get_option( 'americloud_user_id' ) );
			$pin        = trim( give_get_option( 'americloud_pin' ) );

			$type = $type == "TRUE" ? true : false;

			$PaymentProcessor = new \markroland\Converge\ConvergeApi(
				$merchantId,
				$userId,
				$pin,
				$type
			);

			$payment_data = [
				'ssl_amount'     => $data['price'],
				'ssl_first_name' => $data['fname'],
				'ssl_last_name'  => $data['lname'],
			];

			$payment_data['ssl_cvv2cvc2_indicator'] = 1;
			$payment_data['ssl_card_present']       = "N";
			$payment_data['ssl_card_number'] = $data['card_number'];
			$payment_data['ssl_cvv2cvc2']    = $data['cvv2cvc2'];
			$payment_data['ssl_exp_date']    = $data['exp_date'];
			$payment_data['ssl_city']        = $data['card_city'];
			$payment_data['ssl_state']       = $data['card_state'];
			$payment_data['ssl_avs_zip']     = $data['card_zip'];
			$payment_data['ssl_avs_address'] = $data['card_address'];
			$payment_data['ssl_address2']    = $data['card_address_2'];
			$payment_data['ssl_country']     = $data['billing_country'];
			$payment_data['ssl_email']       = $data['user_email'];
			$payment_data['ssl_description'] = $data['form_name'];

			// echo "<pre>";
			// print_r($payment_data);
			// echo "</pre>";

			// calculate first data of next month
			//$firstDayNextMonth = date('m/d/Y', strtotime('first day of next month'));

			// calculate next day
			$nextDay = date('m/d/Y', strtotime('+1 day'));
			//$sameDay = date(m/d/Y);
			// Submit a recurring purchase with CreditCard
			if(isset($data['recurring_donation'])) {
				$payment_data['ssl_billing_cycle'] = 'MONTHLY';
				$payment_data['ssl_next_payment_date'] = $nextDay;
				return $PaymentProcessor->ccaddrecurring( $payment_data );
			}
			// Submit a one time purchase with CreditCard
			else {
				return $PaymentProcessor->ccsale( $payment_data );
			}
		}

		public function check_admin_keys() {
			$settings = array(
				'type'       => trim( give_get_option( 'americloud_account_type' ) ),
				'merchantID' => trim( give_get_option( 'americloud_merchant_id' ) ),
				'userId'     => trim( give_get_option( 'americloud_user_id' ) ),
				'pin'        => trim( give_get_option( 'americloud_pin' ) )
			);

			if ( in_array( '', $settings ) ) {
				return false;
			} else {
				return true;
			}
		}

		/**
		* Process the POST Data for the Credit Card Form, if a token was not supplied.
		*
		*/
		public function verify_cc_post_data() {
			if ( ! isset( $_POST['card_name'] ) || strlen( trim( $_POST['card_name'] ) ) == 0 ) {
				give_set_error( 'no_card_name', esc_html__( 'Please enter a name for the credit card.', 'give-americloud' ) );
			}

			if ( ! isset( $_POST['card_number'] ) || strlen( trim( $_POST['card_number'] ) ) == 0 ) {
				give_set_error( 'no_card_number', esc_html__( 'Please enter a credit card number.', 'give-americloud' ) );
			}

			if ( ! isset( $_POST['card_cvc'] ) || strlen( trim( $_POST['card_cvc'] ) ) == 0 ) {
				give_set_error( 'no_card_cvc', esc_html__( 'Please enter a CVC/CVV for the credit card.', 'give-americloud' ) );
			}

			if ( ! isset( $_POST['card_exp_month'] ) || strlen( trim( $_POST['card_exp_month'] ) ) == 0 ) {
				give_set_error( 'no_card_exp_month', esc_html__( 'Please enter an expiration month.', 'give-americloud' ) );
			}

			if ( ! isset( $_POST['card_exp_year'] ) || strlen( trim( $_POST['card_exp_year'] ) ) == 0 ) {
				give_set_error( 'no_card_exp_year', esc_html__( 'Please enter an expiration year.', 'give-americloud' ) );
			}
		}

	}

	function prefix_custom_notice( $notice, $id, $status, $meta ) {

    if ( 'failed' === $status ) {
        return give_output_error( __( 'Error: ' . $_SESSION['errorName'] ), false, 'failed' );
    }

		else if ( 'publish' === $status ) {
				return give_output_error( __( 'Thank you for your donation!' ), false, 'success' );
		}

		else if ( 'pending' === $status ) {
				return give_output_error( __( 'Your monthly donation has been received!' ), false, 'success' );
		}

    return $notice;
 	}

 	add_filter( 'give_receipt_status_notice', 'prefix_custom_notice', 10, 4 );


	function give_recurring_checkbox() {
		?>

		<div style="margin: 0 0 25px; display: block; line-height: 1em; clear: both; cursor: pointer;">
			<input name="recurring-donation" type="checkbox" value="yes"/>
			<label for="recurring"> Make this donation monthly</label>
		</div>

		<?php
		return true;
	}

	$recurring_enabled = give_get_option('americloud_enable_recurring');
	// Show Recurring checkbox if user has option enabled
	if ($recurring_enabled) {
		add_action( 'give_after_donation_levels', 'give_recurring_checkbox', 1, 2 );
	}

	new Give_AmeriCloudPayments_Gateway();

endif;
