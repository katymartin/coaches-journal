<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Class Give_AmeriCloudPayments_Gateway.
 */
if (!class_exists('Give_AmeriCloudPayments_ACH')):

    class Give_AmeriCloudPayments_ACH
    {
        /**
         * Give_AmeriCloudPayments_Gateway constructor.
         */
        function __construct()
        {
            add_action( 'give_gateway_americloudpayments_ach', array( $this, 'process_ach_payment' ) );
        }

        /**
         * Process user submission
         *
         * @param $payment_data
         */
        public function process_ach_payment($payment_data)
        {

            $recurring_enabled = give_get_option('americloud_enable_recurring');

      			// get recurring check box
      			if ($recurring_enabled) {
      					$recurring_donation = $_POST['recurring-donation'];
      			}

            // Validate nonce.
            give_validate_nonce($payment_data['gateway_nonce'], 'give-gateway');

            $payment_id = $this->give_create_ach_payment($payment_data);
            $data = [];

            // Check payment.
            if (!empty($payment_id)) {

                $data['fname'] = empty($payment_data['user_info']['first_name']) ? '' : $payment_data['user_info']['first_name'];
                $data['lname'] = empty($payment_data['user_info']['last_name']) ? '' : $payment_data['user_info']['last_name'];
                $data['price'] = empty($payment_data['price']) ? '' : $payment_data['price'];
                $data['payment_type'] = empty($payment_data['post_data']['payment_type']) ? 0 : $payment_data['post_data']['payment_type'];
                $data['routing_number'] = empty($payment_data['post_data']['routing_number']) ? '' : $payment_data['post_data']['routing_number'];
                $data['account_number'] = empty($payment_data['post_data']['account_number']) ? '' : $payment_data['post_data']['account_number'];
                $data['company_name'] = empty($payment_data['post_data']['company_name']) ? '' : $payment_data['post_data']['company_name'];
                $data['bank_ac_type'] = $payment_data['post_data']['bank_account_type'];

                $data['card_address']    = empty( $payment_data['post_data']['card_address'] ) ? '' : $payment_data['post_data']['card_address'];
        				$data['card_address_2']  = empty( $payment_data['post_data']['card_address_2'] ) ? '' : $payment_data['post_data']['card_address_2'];
        				$data['card_city']       = empty( $payment_data['post_data']['card_city'] ) ? '' : $payment_data['post_data']['card_city'];
        				$data['card_zip']        = empty( $payment_data['post_data']['card_zip'] ) ? '' : $payment_data['post_data']['card_zip'];
        				$data['card_state']      = empty( $payment_data['post_data']['card_state'] ) ? '' : $payment_data['post_data']['card_state'];
        				$data['billing_country'] = empty( $payment_data['post_data']['billing_country'] ) ? '' : $payment_data['post_data']['billing_country'];

                $data['user_email'] = $payment_data['post_data']['give_email'];
                $data['form_name'] = $payment_data['post_data']['give-form-title'];

                if ($recurring_enabled) {
        						$data['recurring_donation'] = $recurring_donation;
        				}

                $response = $this->give_process_ach_payment_converge($data);

                // echo "<pre>";
                // print_r($response);
                // echo "</pre>";

                if ( $response['ssl_result_message'] == 'APPROVAL' ) {
        						give_set_payment_transaction_id( $payment_id, $response['ssl_txn_id'] );
        						give_insert_payment_note( $payment_id, 'Bank Account No.: ' . $response['ssl_bank_account_number']);
                    give_insert_payment_note( $payment_id, 'Routing No.: ' . $response['ssl_aba_number']);
        						give_insert_payment_note( $payment_id, 'Result: ' . $response['ssl_result_message']);
        						give_update_payment_status( $payment_id, 'publish' ); // Completed
        				}
        				// recurring response
        				else if ( $response['ssl_result_message'] == 'SUCCESS' ) {
        						give_set_payment_transaction_id( $payment_id, $response['ssl_recurring_id'] );
        						give_insert_payment_note( $payment_id, 'Account No.: ' . $response['ssl_card_number']);
        						give_insert_payment_note( $payment_id, 'Next Payment Date: ' . $response['ssl_next_payment_date']);
        						give_insert_payment_note( $payment_id, 'Billing Cycle: ' . $response['ssl_billing_cycle']);
        						give_update_payment_status( $payment_id, 'pending' ); // Completed
        				}
        				else if ( isset( $response['errorCode'] ) ) {
        						give_set_payment_transaction_id( $payment_id, 'Error' );
        						give_insert_payment_note( $payment_id, 'Error Code: ' . $response['errorCode']);
        						give_insert_payment_note( $payment_id, 'Error Name: ' . $response['errorName']);
        						give_insert_payment_note( $payment_id, 'Error Message: ' . $response['errorMessage']);
        						give_update_payment_status( $payment_id, 'failed' ); // Failed
        						session_start();
        						$_SESSION['errorName'] = $response['errorName'];
        			  }
        				else {
        						give_set_payment_transaction_id( $payment_id, 'Error' );
        						give_insert_payment_note( $payment_id, 'Error: ' . $response['errorName']);
        						give_update_payment_status( $payment_id, 'failed' ); // Failed
        						session_start();
        						$_SESSION['errorName'] = $response['errorName'];
        				}

            } else {
                // Record the error.
                give_record_gateway_error(
                    esc_html__('Payment Error', 'give'),
                    sprintf(
                    /* translators: %s: payment data */
                        esc_html__('Payment creation failed before sending donor to payment gateway. Payment data: %s', 'give'),
                        json_encode($payment_data)
                    ),
                    $payment_id
                );
                // Problems? Send back.
                give_send_back_to_checkout('?payment-mode=' . $payment_data['post_data']['give-gateway']);
            }

            // Redirect after successful donation
            give_send_to_success_page();
            session_destroy();
        }

        /**
         * Create Donate object for GIVE
         * @param $payment_data
         * @return bool|int
         */
        private function give_create_ach_payment($payment_data)
        {
            // Collect payment data.
            $insert_payment_data = array(
                'price' => $payment_data['price'],
                'give_form_title' => $payment_data['post_data']['give-form-title'],
                'give_form_id' => intval($payment_data['post_data']['give-form-id']),
                'give_price_id' => isset($payment_data['post_data']['give-price-id']) ? $payment_data['post_data']['give-price-id'] : '',
                'date' => $payment_data['date'],
                'user_email' => $payment_data['user_email'],
                'purchase_key' => $payment_data['purchase_key'],
                'currency' => give_get_currency(),
                'user_info' => $payment_data['user_info'],
                'status' => 'pending',
                'gateway' => 'americloudpayments_ach',
            );

            return give_insert_payment($insert_payment_data);
        }


        /**
         * Process payment to API
         * @param $data
         * @internal param $live_mode
         */
        public function give_process_ach_payment_converge($data)
        {
            $type       = trim( give_get_option( 'americloud_account_type' ) );
            $merchantId = trim( give_get_option( 'americloud_merchant_id' ) );
            $userId     = trim( give_get_option( 'americloud_user_id' ) );
            $pin        = trim( give_get_option( 'americloud_pin' ) );

            $type = $type == "TRUE" ? true : false;

            $PaymentProcessor = new \markroland\Converge\ConvergeApi(
                $merchantId,
                $userId,
                $pin,
                $type
            );

            $payment_data = [
                'ssl_amount' => $data['price'],
                'ssl_first_name' => $data['fname'],
                'ssl_last_name' => $data['lname'],
            ];

            $payment_data['ssl_aba_number'] = $data['routing_number'];
            $payment_data['ssl_bank_account_number'] = $data['account_number'];
            $payment_data['ssl_bank_account_type'] = $data['bank_ac_type'];
            $payment_data['ssl_agree'] = 1;
            $payment_data['ssl_email'] = $data['user_email'];
            $payment_data['ssl_description'] = $data['form_name'];
            if ($data['bank_ac_type'] == 1) {
                $payment_data['ssl_company'] = $data['company_name'];
            }

            // calculate first data of next month
      			//$firstDayNextMonth = date('m/d/Y', strtotime('first day of next month'));

            // calculate next day
      			$nextDay = date('m/d/Y', strtotime('+1 day'));

      			// Submit a recurring purchase with eCheck
      			if(isset($data['recurring_donation'])) {
      				$payment_data['ssl_billing_cycle'] = 'MONTHLY';
      				$payment_data['ssl_next_payment_date'] = $nextDay;
      				return $PaymentProcessor->ecsaddrecurring( $payment_data );
      			}
      			// Submit a one time purchase with eCheck
      			else {
      				return $PaymentProcessor->ecspurchase($payment_data);
      			}

        }


        public function check_admin_keys()
        {
            $settings = array(
                'type' => trim(give_get_option('americloud_account_type')),
                'merchantID' => trim(give_get_option('americloud_merchant_id')),
                'userId' => trim(give_get_option('americloud_user_id')),
                'pin' => trim(give_get_option('americloud_pin'))
            );

            if (in_array('', $settings)) {
                return FALSE;
            } else return TRUE;
        }

    }

    function prefix_custom_notice_ach( $notice, $id, $status, $meta ) {

      if ( 'failed' === $status ) {
          return give_output_error( __( 'Error: ' . $_SESSION['errorName'] ), false, 'failed' );
      }

      else if ( 'publish' === $status ) {
          return give_output_error( __( 'Thank you for your donation!' ), false, 'success' );
      }

      else if ( 'pending' === $status ) {
          return give_output_error( __( 'Your monthly donation has been received!' ), false, 'success' );
      }
      return $notice;
   	}
   	add_filter( 'give_receipt_status_notice', 'prefix_custom_notice_ach', 10, 4 );

    new Give_AmeriCloudPayments_ACH();

endif;
