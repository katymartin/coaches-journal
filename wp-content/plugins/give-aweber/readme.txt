=== Give - AWeber ===
Contributors: wordimpress
Tags: give, aweber, newsletter, email marketing
Requires at least: 4.2
Tested up to: 4.7
Stable tag: 1.0.2
License: GPLv3
License URI: https://opensource.org/licenses/GPL-3.0

Easily integrate AWeber opt-ins within your Give donation forms.

== Description ==

This plugin requires the Give plugin activated to function properly. When activated, it adds a email marketing integration with AWeber.

== Installation ==

= Minimum Requirements =

* WordPress 4.2 or greater
* PHP version 5.3 or greater
* MySQL version 5.0 or greater

= Automatic installation =

Automatic installation is the easiest option as WordPress handles the file transfers itself and you don't need to leave your web browser. To do an automatic install of Give Constant Contact, log in to your WordPress dashboard, navigate to the Plugins menu and click Add New. Upload the zip file you downloaded from your account page on Givewp.com - this will automatically upload the plugin. Once it's done, click "Activate" and you're all set!

= Manual installation =

The manual installation method involves downloading our donation plugin and uploading it to your server via your favorite FTP application. The WordPress codex contains [instructions on how to do this here](http://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation).

= Updating =

Automatic updates should work like a charm; as always though, ensure you backup your site just in case.

== Changelog ==

= 1.0.2 =
* New: The plugin now checks to see if Give is active and up to the minimum version required to run the plugin - https://github.com/WordImpress/Give-Constant-Contact/issues/5
* Tweak: Plugin updated to use new hooks names found in Give 1.7

= 1.0.1 =
* Fix: The refresh email lists button hangs when clicked - https://github.com/WordImpress/Give-AWeber/issues/2

= 1.0 =
* Initial plugin release. Yippee!

